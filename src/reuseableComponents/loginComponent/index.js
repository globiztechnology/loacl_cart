import React from 'react';
import globalStyles from "../../globalStyles";
import { View, Image, KeyboardAvoidingView, Text } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from '../../globalStyles/colors';
import login from '../../screens/loginScreen/login';

export const LoginComponent = (props) => {

    return (
        <KeyboardAvoidingView style={[globalStyles.subMainCont]}>
            <View style={login.logoMain}>
                <View style={globalStyles.view}>
                    <View style={login.logoCon}>
                        <View style={[globalStyles.viewRow]}>
                            <Image style={globalStyles.imageStyle} source={require('../../assets/images/logo.png')} />
                        </View>
                    </View>
                </View>
            </View>
            <View style={login.inputMain}>
                <View style={[globalStyles.subMainCont, { justifyContent: 'center' }]}>
                    <View style={[login.inputCon]}>
                        <View style={globalStyles.inputStyle}>
                            {/* <Icon name="envelope-o" size={30} color='#3c3c3c' /> */}
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/phone.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Mobile Number'}
                                maxLength={10}
                                keyboardType={"number-pad"}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.mobile}
                                onChangeText={(mobile) => props.onChange('mobile',mobile)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={[login.inputCon]}>
                        <View style={globalStyles.inputStyle}>
                            {/* <Icon name="envelope-o" size={30} color='#3c3c3c' /> */}
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/lock.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Password'}
                                password={true}
                                textContentType={'password'}
                                secureTextEntry={true}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.password}
                                onChangeText={(password) => props.onChange('password',password)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                </View>
            </View>
            <View style={login.buttonMain}>
                <View style={[globalStyles.view, { justifyContent: 'flex-end' }]}>
                    <TouchableOpacity style={[globalStyles.buttonStyle2, { paddingHorizontal: hp('7%') }]} onPress={props.handleLogin}>
                        <Text style={globalStyles.buttonText}>login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </KeyboardAvoidingView>
    )
}
export default LoginComponent