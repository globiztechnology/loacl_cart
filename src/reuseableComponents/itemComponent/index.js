import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image, Dimensions, Text, TouchableOpacityBase } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import { Picker } from '@react-native-community/picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import colors from '../../globalStyles/colors';
import signup from '../../screens/customer/signup/signup';

export const ItemComponent = (props) => {
    console.log(props.item);
    return (
        <View style={{ height: hp('38%'), paddingVertical: hp('2%') }}>
            <View style={{ flex: 1, backgroundColor: '#f4f4f4' }}>
                <View style={[signup.inputMain]}>
                    <View style={[globalStyles.inputStyle]}>
                        <Icon2 name="hamburger" size={18} color={'#3c3c3c'} />
                        <TextInput
                            style={globalStyles.input}
                            placeholder={'Item Name'}
                            placeholderTextColor={colors.inputicons}
                            value={props.item.item_name}
                            onChangeText={name => props.onChangeArray('item_name', name, props.index)}
                            selectionColor={colors.primaryColor}
                        />
                    </View>
                </View>

                <View style={signup.inputMain}>
                    {/* <View style={{ flex: 1, flexDirection: 'row' }}> */}
                    <View style={[globalStyles.inputStyle]}>
                        <Icon2 name="balance-scale" size={18} color={'#3c3c3c'} />
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <TextInput
                                style={[globalStyles.input, { flex: .7 }]}
                                placeholder={'Weight'}
                                keyboardType={'numeric'}
                                placeholderTextColor={colors.inputicons}
                                value={props.item.weight}
                                onChangeText={weight => props.onChangeArray('weight', weight, props.index)}
                                selectionColor={colors.primaryColor}
                            />
                            <Picker style={{ flex: .3}}
                                selectedValue={props.item.weight_form}
                                mode={"dropdown"}
                                inputStyle={{}}
                                onValueChange={(itemValue, itemPosition) => {
                                    props.onChangeArray('weight_form', itemValue, props.index);
                                    console.log(itemValue)
                                }}
                            >
                                <Picker.Item label="Kg" value="Kg" color={colors.inputicons}/>
                                <Picker.Item label="g" value="g" color={colors.inputicons}/>
                            </Picker>
                        </View>

                    </View>
                
                </View>
            <View style={signup.inputMain}>
                <View style={[globalStyles.inputStyle]}>
                    <Icon2 name="weight-hanging" size={18} color={'#3c3c3c'} />
                    <TextInput
                        style={globalStyles.input}
                        placeholder={'Quantity'}
                        placeholderTextColor={colors.inputicons}
                        keyboardType={'number-pad'}
                        value={props.item.quantity}
                        onChangeText={quant => props.onChangeArray('quantity', quant, props.index)}
                        selectionColor={colors.primaryColor}
                    />
                </View>
            </View>

    <View style={[{
        height: hp('10%'),
        bottom: 0,
        paddingHorizontal: hp('2%'),
        paddingVertical: hp('1%'),
    }]}>
        <View style={[globalStyles.inputStyle]}>
            <Icon name="comment-o" size={18} color={'#3c3c3c'} />
            <TextInput
                multiline={true}
                style={globalStyles.input}
                placeholder={'Comment(If any)'}
                placeholderTextColor={colors.inputicons}
                value={props.item.notes}
                onChangeText={note => props.onChangeArray('notes', note, props.index)}
                selectionColor={colors.primaryColor}
            />
        </View>
    </View>
            </View >
        </View >);
}
export default ItemComponent