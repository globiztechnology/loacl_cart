import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image, Dimensions,TouchableOpacity, Text } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {  TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign'
import colors from '../../globalStyles/colors';
import signup from '../../screens/customer/signup/signup';
import DateTimePickerModal from "react-native-modal-datetime-picker";

//import ModalDropdown from 'react-native-modal-dropdown';

export const ShopSignupComp = (props) => {
    return (
        <View style={globalStyles.subMainCont}>
            <View style={{ height: hp('45%') }}>
                <View style={globalStyles.subMainCont}>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/category.png')} resizeMode='stretch' />
                            <Picker style={globalStyles.input}
                                selectedValue={props.state.category_id}
                                itemStyle={{ color: colors.inputicons }}
                                mode={"dropdown"}
                                onValueChange={(itemValue, itemPosition) => props.onChange('category_id', itemValue)}
                            >
                                <Picker.Item enabled={false} label="Category" />
                                {props.state.category.map((item, index) => {
                                    return (< Picker.Item label={item.category_name} value={item.id} key={item.id} />);
                                })}
                            </Picker>
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/shop.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Shop Name'}
                                value={props.state.shop_name}
                                onChangeText={shop_name => props.onChange('shop_name', shop_name)}
                                placeholderTextColor={colors.inputicons}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={[globalStyles.inputStyle, { marginRight: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/time.png')} resizeMode='stretch' />
                             
                                <TouchableOpacity style={[globalStyles.input,{justifyContent:'center'}]} onPress={()=>props.showDatePicker()}>
                            <Text style={{color:colors.inputicons,fontSize:hp('1.9%'),paddingBottom:0}}>{props.state.shop_open_time}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={props.state.time}
                                    mode="time"
                                    is24Hour={false}
                                    onConfirm={props.handleConfirm}
                                    onCancel={props.hideDatePicker}
                                />
                            </View>
                            <View style={[globalStyles.inputStyle, { marginLeft: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/time.png')} resizeMode='stretch' />
                                <TouchableOpacity style={[globalStyles.input,{justifyContent:'center'}]} onPress={()=>props.showDatePicker1()}>
                            <Text style={{color:colors.inputicons,fontSize:hp('1.9%'),paddingBottom:0}}>{props.state.shop_close_time}</Text>
                                </TouchableOpacity>
                                <DateTimePickerModal
                                    isVisible={props.state.close}
                                    mode="time"
                                    is24Hour={false}
                                    onConfirm={props.handleShopClose}
                                    onCancel={props.hideDatePicker1}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/bank.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Bank Name'}
                                value={props.state.bank_name}
                                onChangeText={bank_name => props.onChange('bank_name', bank_name)}
                                placeholderTextColor={colors.inputicons}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/account.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Account Number'}
                                keyboardType={'number-pad'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.account_no}
                                onChangeText={account_no => props.onChange('account_no', account_no)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/account.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                keyboardType={'number-pad'}
                                placeholder={'Confirm Account Number'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.confirm_account}
                                onChangeText={confirm_account => props.onChange('confirm_account', confirm_account)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}
export default ShopSignupComp