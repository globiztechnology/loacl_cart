import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image, Dimensions } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome'
import colors from '../../globalStyles/colors';
import signup from '../../screens/customer/signup/signup';
import {
    Dropdown
}
    from 'react-native-material-dropdown';
//import ModalDropdown from 'react-native-modal-dropdown';

export const SignUpComponent = (props) => {
    let data = [{
        value: 'Banana',
    }, {
        value: 'Mango',
    }, {
        value: 'Pear',
    }];

    return (
        <View style={globalStyles.subMainCont}>
            <View style={{ height: hp('18%') }}>
                <View style={globalStyles.view}>
                    <View style={{
                        height: hp('15%'),
                        width: hp('15%'),
                        borderRadius: hp('7.5%'),
                        overflow: 'hidden',
                    }}>
                        <Image style={[globalStyles.imageStyle,]} source={props.state.image == '' ?
                            require('../../assets/images/profile2.png')
                            : { uri: props.state.image.uri }}
                        // : { uri: 'data:image/jpeg;base64,'+props.state.image.data }} 
                        />
                    </View>
                    <View style={{
                        height: hp('4%'),
                        width: hp('4%'),
                        position: 'absolute',
                        zindex: 100,
                        bottom: hp('1%'),
                        left: '53%',
                        overflow: 'hidden',
                        borderRadius: hp('2%'),
                        backgroundColor: colors.secondColor
                    }}>
                        <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }} onPress={props.selectImage}>
                            <Image style={{ height: '50%', width: '50%', resizeMode: 'stretch' }} source={require('../../assets/images/camera.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            <View style={{ height: hp('56.1%') }}>
                <View style={globalStyles.subMainCont}>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>

                            {/* <Icon style={globalStyles.inputIcon} name="user-o" size={22} color={'#3c3c3c'} /> */}
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/name.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Name'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.name}
                                onChangeText={name => props.onChange('name', name)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            {/* <Icon style={globalStyles.inputIcon} name="mobile-phone" size={25} color={'#3c3c3c'} /> */}
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/phone.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Phone Number'}
                                keyboardType={'number-pad'}
                                maxLength={10}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.mobile}
                                onChangeText={mobile => props.onChange('mobile', mobile)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            {/* <Icon style={globalStyles.inputIcon} name="mobile-phone" size={25} color={'#3c3c3c'} /> */}
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/email.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Email'}
                                keyboardType={'email-address'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.email}
                                onChangeText={email => props.onChange('email', email)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/addar.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Aadhar Number(Optional)'}
                                keyboardType={'number-pad'}
                                maxLength={12}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.aadhar_no}
                                onChangeText={aadhar_no => props.onChange('aadhar_no', aadhar_no)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/gender.png')} resizeMode='stretch' />
                            <Picker style={globalStyles.input}

                                selectedValue={props.state.gender}
                                itemStyle={{ color: colors.inputicons }}
                                mode={"dropdown"}
                                onValueChange={(itemValue, itemPosition) => {
                                    props.onChange('gender', itemValue);
                                    console.log(props.state.gender)
                                }}
                            >
                                <Picker.Item enabled={false} label="Gender" />
                                <Picker.Item label="Male" value="Male" />
                                <Picker.Item label="Female" value="Female" />
                            </Picker>
                        </View>
                    </View>

                    <View style={signup.inputMain}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={[globalStyles.inputStyle, { marginRight: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/lock.png')} resizeMode='stretch' />
                                <TextInput
                                    style={globalStyles.input}
                                    placeholder={'Password'}
                                    secureTextEntry={true}
                                    textContentType={"password"}
                                    placeholderTextColor={colors.inputicons}
                                    value={props.state.password}
                                    onChangeText={password => props.onChange('password', password)}
                                    selectionColor={colors.primaryColor}
                                />
                            </View>
                            <View style={[globalStyles.inputStyle, { marginLeft: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/lock.png')} resizeMode='stretch' />
                                <TextInput
                                    style={globalStyles.input}
                                    placeholder={'Confirm Password'}
                                    secureTextEntry={true}
                                    textContentType={"password"}
                                    placeholderTextColor={colors.inputicons}
                                    value={props.state.confirmPass}
                                    onChangeText={confirmPass => props.onChange('confirmPass', confirmPass)}
                                    selectionColor={colors.primaryColor}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/place.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Place'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.address}
                                onChangeText={address => props.onChange('address', address)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={[globalStyles.inputStyle, { marginRight: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/state.png')} resizeMode='stretch' />
                                <Picker style={globalStyles.input}
                                selectedValue={props.state.district}
                                itemStyle={{ color: colors.inputicons}}
                                mode={"dropdown"}
                                onValueChange={(itemValue, itemPosition) => props.onChange('district', itemValue)}
                            >
                                <Picker.Item enabled={false} label="District" />
                                {props.state.cities.map((item, index) => {
                                    return (< Picker.Item label={item.name} value={item.id} key={item.id} />);
                                })}
                            </Picker>
                              
                            </View>
                            <View style={[globalStyles.inputStyle, { marginLeft: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/state.png')} resizeMode='stretch' />
                                <Picker style={globalStyles.input}
                                selectedValue={props.state.stat}
                                itemStyle={{ color: colors.inputicons}}
                                mode={"dropdown"}
                                onValueChange={(itemValue, itemPosition) => {
                                
                                props.onChange('stat', itemValue)
                            }}
                            >
                                <Picker.Item enabled={false} label="State"/>
                                {props.state.states.map((item, index) => {
                                    return (< Picker.Item label={item.name} value={item.id} key={item.id} />);
                                })}
                            </Picker>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}
export default SignUpComponent