import React from 'react';
import { View, Image, Picker, Dimensions, Text,StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import globalStyles from '../../globalStyles';
import { connect } from 'react-redux';
import colors from '../../globalStyles/colors';

export const OrderListComponent = (props) => {
    console.log(props);
    return (
        <TouchableOpacity style={{ height: hp('10.5%'), width: '100%', paddingVertical: hp('.8%') }}
        onPress={()=>props.detailOrder()}>
            <View style={{ flex: 1, backgroundColor: '#eaeaea',paddingLeft:hp('1.5%') }}>
                <View style={globalStyles.mainCont}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ flex: .7, flexDirection: 'column' }}>
                        <View style={{ flex: .45, }}>
                            <View style={[globalStyles.subMainCont, { flexDirection: 'row' }]}>
                                <View style={{ flex: .4, flexDirection: 'row', alignItems: 'center' }}>
                                    <Text style={{
                                        fontSize: hp('1.8%'),
                                        textAlign: 'center',
                                        color: '#999999',
                                    }}>25 April 2020</Text>
                                </View>
                                <View style={{ flex: .1, flexDirection: 'column',alignItems: 'center',paddingVertical:hp('1%')}}>
                                    <View style={{width:hp('.1%'),height:'100%',backgroundColor:'#999999'}}></View>
                                </View>
                                <View style={{ flex: .5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start',paddingLeft:hp('1%') }}>
                                    <Text  style={{
                                        fontSize: hp('1.8%'),
                                        textAlign: 'center',
                                        color: '#999999',
                                    }}>04:00 PM</Text>
                                </View>
                            </View>

                        </View>
                        <View style={{ flex: .55, flexDirection: 'row', alignItems: 'center'}}>
                            <Text style={{
                                        fontSize: hp('2.5%'),
                                        fontWeight:'bold',
                                        textAlign: 'center',
                                        color: '#333',
                                    }}>Order Id: {props.item.id}</Text>
                        </View>
                    </View>
                    <View style={{ flex: .3, flexDirection: 'row',paddingLeft:hp('1.2%') }}>
                                <View style={globalStyles.subMainCont}>
                                   {props.status(props.item)}
                                </View>
                    </View>
                </View>
                </View>
            </View>
        </TouchableOpacity>
    );
}



export default (OrderListComponent);