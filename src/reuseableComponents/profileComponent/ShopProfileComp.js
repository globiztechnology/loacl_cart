import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image, Dimensions } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign'
import colors from '../../globalStyles/colors';
import signup from '../../screens/customer/profile/signup';
//import ModalDropdown from 'react-native-modal-dropdown';

export const ShopProfileComp = (props) => {
    let category;
    return (
        <View style={globalStyles.subMainCont}>
            <View style={{ height: hp('38%') }}>
                <View style={globalStyles.subMainCont}>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/shop.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Shop Name'}
                                value={props.state.shop_name}
                                onChangeText={shop_name => props.onChange('shop_name', shop_name)}
                                placeholderTextColor={colors.inputicons}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={[globalStyles.inputStyle, { marginRight: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/time.png')} resizeMode='stretch' />
                                <TextInput
                                    style={globalStyles.input}
                                    keyboardType={'default'}
                                    placeholder={'Shop Open Time'}
                                    placeholderTextColor={colors.inputicons}
                                    value={props.state.shop_open_time}
                                    onChangeText={shop_open_time => props.onChange('shop_open_time', shop_open_time)}
                                    selectionColor={colors.primaryColor}
                                />
                            </View>
                            <View style={[globalStyles.inputStyle, { marginLeft: hp('1.5%') }]}>
                                <Image style={globalStyles.inputIcon} source={require('../../assets/images/time.png')} resizeMode='stretch' />
                                <TextInput
                                    style={globalStyles.input}
                                    keyboardType={'default'}
                                    placeholder={'Shop Close Time'}
                                    value={props.state.shop_close_time}
                                    onChangeText={shop_close_time => props.onChange('shop_close_time', shop_close_time)}
                                    placeholderTextColor={colors.inputicons}
                                    selectionColor={colors.primaryColor}
                                />
                            </View>

                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/bank.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Bank Name'}
                                value={props.state.bank_name}
                                onChangeText={bank_name => props.onChange('bank_name', bank_name)}
                                placeholderTextColor={colors.inputicons}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/account.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                placeholder={'Account Number'}
                                keyboardType={'number-pad'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.account_no}
                                onChangeText={account_no => props.onChange('account_no', account_no)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    {/* <View style={signup.inputMain}>
                        <View style={[globalStyles.inputStyle]}>
                            <Image style={globalStyles.inputIcon} source={require('../../assets/images/account.png')} resizeMode='stretch' />
                            <TextInput
                                style={globalStyles.input}
                                keyboardType={'number-pad'}
                                placeholder={'Confirm Account Number'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.confirm_account}
                                onChangeText={confirm_account => props.onChange('confirm_account', confirm_account)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View> */}
                </View>
            </View>
        </View>
    );
}
export default ShopProfileComp