import React, { Component } from 'react';
import { View, Image,Text, StyleSheet} from 'react-native';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
  } from 'react-native-indicators';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../globalStyles/colors';

export default class Loader extends Component {
    _renderLoader = () => {
      if (this.props.isLoader) return (
        <View style={styles.background}>
        <UIActivityIndicator size={45} color={colors.primaryColor} />
        </View>
      )
      else return null;
    }
  render () {
      return (
        this._renderLoader()
      )
    }
  }
const styles = StyleSheet.create ({
    background: {
      backgroundColor:colors.white ,
      opacity:0.5,
      flex: 1,
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      alignItems: 'center',
      justifyContent: 'center'
    }
  });
