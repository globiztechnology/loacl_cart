import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image, Dimensions, Text, TouchableOpacityBase } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import Icon3 from 'react-native-vector-icons/Feather';
import SearchableDropdown from 'react-native-searchable-dropdown';
import colors from '../../globalStyles/colors';
import { Picker } from '@react-native-community/picker';
import signup from '../../screens/customer/signup/signup';
import CreateOrderScreen from '../../screens/customer/createOrder';
import ItemComponent from '../itemComponent';


export const CreateOrderComponent = (props) => {

    return (
        <View style={globalStyles.subMainCont}>
            <View style={{ height: hp('20%') }}>
                <View style={[globalStyles.subMainCont, { backgroundColor: '#f4f4f4' }]}>
                    <View style={[signup.inputMain]}>
                        <View style={[globalStyles.inputStyle]}>
                            <Icon name="th-list" size={18} color={'#3c3c3c'} />
                            <View
                                style={[globalStyles.input, { flexDirection: 'row' }]}>
                                <View style={{ width: '90%' }}>
                                    <Text style={{ color: colors.inputicons }}>{props.state.shopCategory}</Text>
                                </View>
                                <TouchableOpacity style={{ paddingHorizontal: hp('1%') }} onPress={() => { props.categorySelect() }}>
                                    <Icon name="caret-down" size={20} color={'#3c3c3c'} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    <View style={[signup.inputMain, { height: hp('7.2%') }]}>
                        <View style={[globalStyles.inputStyle]}>
                            <Icon2 name="store" size={18} color={'#3c3c3c'} />
                            <View
                                style={[globalStyles.input, { flexDirection: 'row' }]}>
                                <View style={{ width: '90%' }}>
                                    <Text style={{ color: colors.inputicons }}>{props.state.shopName}</Text>
                                </View>
                                <TouchableOpacity style={{ paddingHorizontal: hp('1%') }} onPress={() => { props.shopSelect() }}>
                                    <Icon name="caret-down" size={20} color={'#3c3c3c'} />
                                </TouchableOpacity>
                            </View>

                        </View>
                    </View>
                    <View style={[{ height: hp('5.8%'), paddingHorizontal: hp('2%'), paddingVertical: hp('1%') }]}>
                        <View style={[{ flex: 1, flexDirection: 'row' }]}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ fontSize: hp('1.5%') }}>{props.state.shopkeeperName}</Text>
                            </View>
                            {/* <View style={{ flex: .2 }}>
                                <View style={{
                                    height: hp('3%'),
                                    width: hp('3%'),
                                    overflow: 'hidden',
                                    borderRadius: hp('1.5%'),
                                    alignSelf: 'flex-end',
                                    backgroundColor: colors.secondColor
                                }}>
                                    <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}
                                    onPress={props.makeCall}>
                                        <Icon3 name="phone" size={15} color={'#fff'} />
                                    </TouchableOpacity>
                                </View> 
                            </View>*/}
                        </View>
                    </View>
                </View>
            </View>
            { props.state.item.map((item, i) => (
            <ItemComponent item={item} index={i} onChangeArray={(field,value,i)=>props.onChangeArray(field,value,i)}/>))}
            <View style={{ height: hp('4%'), alignItems: 'flex-end', justifyContent: 'center' }}>
                <TouchableOpacity style={{ paddingVertical: hp('1%'), flexDirection: 'row' }} onPress={props.itemCreate}>
                    <Icon3 name='plus' color='#e97657' size={20} />
                    <Text style={{ fontSize: hp('2%'), color: colors.primaryColor, paddingLeft: hp('.5%') }}>Add Item</Text>
                </TouchableOpacity>
            </View>
            <View style={{ height: hp('20%'), paddingVertical: hp('1%') }}>
                <View style={{ flex: 1 }}>
                    <View style={[{
                        height: hp('10%'),
                        bottom: 0,
                        paddingHorizontal: hp('2%'),
                        paddingVertical: hp('1%'),
                    }]}>
                        <View style={[globalStyles.inputStyle]}>
                            <Icon name="sticky-note-o" size={18} color={'#3c3c3c'} />
                            <TextInput
                                multiline={true}
                                style={globalStyles.input}
                                placeholder={'Notes(If any)'}
                                placeholderTextColor={colors.inputicons}
                                value={props.state.notes}
                                onChangeText={notes => props.onChange('notes', notes)}
                                selectionColor={colors.primaryColor}
                            />
                        </View>
                    </View>
                    <View style={[signup.inputMain,{paddingVertical:hp('1%')}]}>
                        <View style={[globalStyles.inputStyle]}>
                            <Icon name="clock-o" size={18} color={'#3c3c3c'} />
                            <Picker style={globalStyles.input}

                                selectedValue={props.state.pickTime}
                                itemStyle={{ color: colors.inputicons }}
                                mode={"dropdown"}
                                onValueChange={(itemValue, itemPosition) => {
                                     props.onChange('pickTime', itemValue);
                                    console.log(itemValue)
                                }}
                            >
                                <Picker.Item enabled={false} label="Order Pickup Timings" />
                                <Picker.Item label="Today" value="Today" />
                                <Picker.Item label="Tomorrow" value="Tomorrow" />
                            </Picker>
                        </View>
                    </View>
                </View>


            </View>
              <View style={{height:hp('10%')}}>
                  <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                            <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                                <TouchableOpacity style={{paddingVertical:hp('1.5%'),
                                paddingHorizontal:hp('7%'),
                                backgroundColor:colors.primaryColor,
                                borderRadius:hp('2.5%')}}
                               >
                                    <Text style={globalStyles.buttonText}>Preview</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1,justifyContent:'center', alignItems:'center'}}>
                                <TouchableOpacity style={{ borderRadius:hp('2.5%'),paddingVertical:hp('1.5%'),paddingHorizontal:hp('7.5%'),backgroundColor:colors.secondColor}} 
                                 onPress={props.submitOrder}>
                                    <Text style={globalStyles.buttonText}>Submit</Text>
                                </TouchableOpacity>
                            </View>
                  </View>
                  </View>                  
        </View>
    );
}
export default CreateOrderComponent