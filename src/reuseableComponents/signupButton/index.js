import React from 'react';
import globalStyles from '../../globalStyles';
import { View, Image,Text} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, TextInput } from 'react-native-gesture-handler';
import colors from '../../globalStyles/colors';
import login from '../../screens/loginScreen/login';


export const SignUpButton = (props) => {
    return (
        <View style={globalStyles.subMainCont}>
            <View style={{ height: hp('10%')}}>
                <View style={globalStyles.viewRow}>
                    <TouchableOpacity style={[globalStyles.buttonStyle2,{paddingHorizontal:hp('6.5%')}]} onPress={props.handleSignUp}>
                        <Text style={globalStyles.buttonText}>Signup</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ height: hp('10%')}}>
                <View style={globalStyles.viewRow}>
                    <Text style={[login.text, { color: colors.inputicons }]}>Already have an account?</Text>
                    <TouchableOpacity onPress={()=>props.navigation.navigate('Login')}>
                        <Text style={[login.text, { color: colors.primaryColor }]}> Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}
export default SignUpButton