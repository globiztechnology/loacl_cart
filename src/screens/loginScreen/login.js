import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    logoMain:{
        flex:.5,
    },
    logoCon:{
        height: '100%',
        width: '60%',
        alignSelf: 'center'
    },
    inputMain:{
        flex:.35
    },
    inputCon:{
    height:hp('7%'),
      paddingHorizontal:hp('2%'),
      paddingVertical:hp('1%')
    },
    buttonMain:{
    flex:.15,
    },
   text:{
       fontSize:hp('1.6%'),
       lineHeight:hp('2%'),
       textAlign:'center'
   }
});