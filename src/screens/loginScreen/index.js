import React, { Component } from 'react';
import globalStyles from '../../globalStyles';
import { ImageBackground, View, Text, StatusBar, KeyboardAvoidingView, Image, ToastAndroid,BackHandler, Alert} from 'react-native';
import colors from '../../globalStyles/colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LoginComponent from '../../reuseableComponents/loginComponent';
import login from './login';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { setUserData, setToken } from '../../redux/actions/user';
import APICaller from '../../utilities/apiCaller';

class LoginScreen extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>Login</Text>
                </View>
            ),
            headerLeft: () => (<View></View>
                // <TouchableOpacity
                //     style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                //     onPress={() => navigation.goBack()}>
                //     {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                //     <Image style={globalStyles.headerIcon} source={require('../../assets/images/back.png')} />
                // </TouchableOpacity>
            ),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            mobile: '',
            password: '',
        };
    }
    onChange = (field, value) => {
        console.log(field, value);
        this.setState({ [field]: value });
    }

    validateFields = () => {
        if (this.state.mobile === '') {
            ToastAndroid.showWithGravity("Please fill the mobile number", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.password === '') {
            ToastAndroid.showWithGravity("Please fill the password", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else {
            return true;
        }
    }

    _login = async () => {
        console.log('LOGIN INFORMATION');
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'login';

        const body = {
            mobile_no: this.state.mobile,
            password: this.state.password
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    await AsyncStorage.setItem(
                        'userData',
                        JSON.stringify(data.data),
                    );
                    await AsyncStorage.setItem('token',
                    JSON.stringify(data.data.token),);
                    this.props.dispatch(setUserData(data.data));
                    this.props.dispatch(setToken(data.data.token));
                    if(data.data.user_type=='customer'){
                        this.props.navigation.navigate('Welcome');
                    }else{
                        this.props.navigation.navigate('ShopOrder');
                    }
                 
                    this.setState({
                        mobile:'',
                        password:''
                    })
                }
                else {
                    ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    };
    handleLogin = async () => {
        const status = await this.validateFields();
        console.log(status)
        if (status) {
            this._login();
        }
        //  else {
        //     Alert.alert("Error", this.state.error);
        // }
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <KeyboardAvoidingView style={globalStyles.subMainCont}>
                    <ImageBackground style={globalStyles.backgroundImage} source={require('../../assets/images/login_bg.png')} resizeMode='stretch'>
                        <View style={{ height: hp('50%'), padding: hp('1%') }}>
                            <LoginComponent
                                state={this.state}
                                onChange={(field, value) => this.onChange(field, value)}
                                handleLogin={() => this.handleLogin()}
                                navigation={this.props.navigation} />
                        </View>
                        <View style={{ height: hp('40%'), padding: hp('1%') }}>
                            <View style={globalStyles.subMainCont}>
                                <View style={{ height: hp('6%') }}>
                                    <View style={globalStyles.viewRow}>
                                        <Text style={[login.text, { color: colors.inputicons }]}>Forgot Password? </Text>
                                        <TouchableOpacity>
                                            <Text style={[login.text, { color: colors.primaryColor }]}>Click here </Text>
                                        </TouchableOpacity>

                                        <Text style={[login.text, { color: colors.inputicons }]}>to recover.</Text>
                                    </View>
                                </View>
                                <View style={{ height: hp('4%'), paddingHorizontal: hp('12%') }}>
                                    <View style={globalStyles.inputStyle}></View>
                                </View>
                                <View style={{ height: hp('12%') }}>
                                    <View style={globalStyles.viewRow}>
                                        <Text style={[login.text, { color: colors.inputicons }]}>Trouble logging in?</Text>
                                    </View>
                                </View>
                                <View style={{ height: hp('16%') }}>
                                    <View style={globalStyles.viewRow}>
                                        <Text style={[login.text, { color: colors.inputicons }]}>Don't have an account yet? </Text>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginAs')}>
                                            <Text style={[login.text, { color: colors.primaryColor }]}>Sign Up</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ImageBackground>
                </KeyboardAvoidingView>
            </View>
        )
    }
}
const mapStateToProps = state => ({
    user: state.UserReducer.user
});
export default connect(mapStateToProps)(LoginScreen);