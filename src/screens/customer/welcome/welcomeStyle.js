import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    welcomeMain:{ flex: .36 },
    buttonMain:{ flex: .16 },
    welcomeSub:{ flex: .3, flexDirection: 'row' },
    textMain:{ flex: .7, alignItems: 'center', justifyContent: 'center'},
    textStyle:{ fontSize: hp('2.2%'), fontWeight: "normal", color: '#665d5d', lineHeight: hp('3%'), textAlign: 'center' },
});