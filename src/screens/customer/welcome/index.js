import React, { Component } from 'react';
import { View, ImageBackground, Image, Text, StatusBar } from 'react-native';
import globalStyles from '../../../globalStyles';
import loginAsStyle from '../../loginAs/loginAsStyle';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import welcomeStyle from './welcomeStyle';
class WelcomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={true} />
                <View style={globalStyles.subMainCont}>
                    <ImageBackground style={globalStyles.backgroundImage} source={require('../../../assets/images/bg.png')} resizeMode='stretch'>
                        <View style={globalStyles.mainCont}>
                            <View style={{ flex: .16 }}></View>
                            <View style={{ flex: .38 }}>
                                <View style={loginAsStyle.subMainCon}>
                                    <View style={loginAsStyle.logoMainCon}>
                                        <View style={loginAsStyle.logoSubCon}>
                                            <View style={loginAsStyle.logoStyle}>
                                                <Image style={globalStyles.imageStyle} resizeMode='stretch' source={require('../../../assets/images/logo.png')} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={welcomeStyle.welcomeMain}>
                                        <View style={globalStyles.view}>
                                            <View style={welcomeStyle.welcomeSub}>
                                                <View style={globalStyles.row}>
                                                    <View style={loginAsStyle.buttonSub}>
                                                        <Text style={loginAsStyle.textStyle}>Welcome</Text>
                                                    </View>
                                                    <View style={[loginAsStyle.buttonSub,{alignItems:'flex-start'}]}>
                                                        <Text style={[loginAsStyle.textStyle, { fontWeight: 'bold',textAlign:'left',textTransform:'capitalize' }]} numberOfLines={1}>{this.props.user.name}!</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={welcomeStyle.textMain}>
                                                <Text style={welcomeStyle.textStyle}>Let's order your household items{'\n'}
                                                from your favorite shop</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={welcomeStyle.buttonMain}>
                                        <View style={globalStyles.view}>
                                            <TouchableOpacity style={[globalStyles.buttonStyle2, { paddingHorizontal: hp('3%') }]} onPress={() => this.props.navigation.navigate('CreateOrder')}>
                                                <Text style={globalStyles.buttonText}>create order</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: .46, }}></View>

                        </View>
                    </ImageBackground>
                </View>
            </View>

        );
    }
}

const mapStateToProps = state => ({
    user: state.UserReducer.user
});
export default connect(mapStateToProps)(WelcomeScreen);