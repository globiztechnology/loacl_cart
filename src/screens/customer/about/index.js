import React, {Component} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TouchableOpacity, ScrollView} from 'react-native-gesture-handler';
import styles from './style';
import globalStyles from '../../../globalStyles';
import colors from '../../../globalStyles/colors';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';

class CustomerAbout extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: colors.primaryColor,
      },
      headerTitle: () => (
        <View style={globalStyles.viewRow}>
          <Text style={globalStyles.headerTitle}>About Localcart</Text>
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginLeft: hp('2%')}]}
          onPress={() => navigation.openDrawer('App')}>
          <Icon3 name="menu" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginRight: hp('2%')}]}
          >
          <Icon3 name="cart-outline" size={20} color="#fff" />
        </TouchableOpacity>
      ),
    };
  };

  render() {
    return (
      <SafeAreaView style={styles.resposiveBox}>
        <ScrollView style={styles.body}>
          <View style={styles.contentSec}>
            <Text style={styles.description}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
            <Text style={styles.heading}>What is Lorem Ipsum?</Text>
            <Text style={styles.description}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of Letraset sheets
              containing Lorem Ipsum passages, and more recently with desktop
              publishing software like Aldus PageMaker including versions of
              Lorem Ipsum.
            </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default CustomerAbout;
