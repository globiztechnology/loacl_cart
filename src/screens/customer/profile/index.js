import React, { Component } from 'react';
import globalStyles from "../../../globalStyles"
import { KeyboardAvoidingView, ImageBackground, View, Text, Image, StatusBar, Alert, ToastAndroid } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import colors from '../../../globalStyles/colors';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfileComponent from '../../../reuseableComponents/profileComponent';
import { connect } from 'react-redux';

class CustomerProfile extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
              backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
              <View style={globalStyles.viewRow}>
                <Text style={globalStyles.headerTitle}>Profile</Text>
              </View>
            ),
            headerLeft: () => (
              <TouchableOpacity
                style={[globalStyles.viewRow, {marginLeft: hp('2%')}]}
                onPress={() => navigation.openDrawer('App')}>
                <Icon3 name="menu" size={30} color="#fff" />
              </TouchableOpacity>
            ),
            headerRight: () => (
              <TouchableOpacity
                style={[globalStyles.viewRow, {marginRight: hp('2%')}]}
                >
                <Icon3 name="cart-outline" size={20} color="#fff" />
              </TouchableOpacity>
            ),
          };
    };
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            name: props.user.name,
            email: props.user.email,
            mobile: props.user.mobile_no,
            aadhar_no: (props.user.aadhar_no!=null)?props.user.aadhar_no:'',
            address: props.user.address_details.address,
            // user_type:'',
            profile_pic:'',
            gender: props.user.gender,
            district: props.user.address_details.district,
            stat: props.user.address_details.state,
            image: '',
            defaultImage: ''
        };
    }
    validateFields = () => {
        console.log("Gender", this.state.gender);
        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var pass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[^\w\d]).*$/;
        if (this.state.name === '') {
            ToastAndroid.showWithGravity("Please fill the name",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile === '') {
            ToastAndroid.showWithGravity("Please fill the mobile number",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile.replace(/\s|\*|\+|\#/g, '').length !== 10) {
            ToastAndroid.showWithGravity("Please enter a valid mobile number",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.email === '') {
            ToastAndroid.showWithGravity("Please fill the email",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (email.test(this.state.email) === false) {
            ToastAndroid.showWithGravity("Email is not correct",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.gender === '' || this.state.gender === undefined) {
            ToastAndroid.showWithGravity("Please select the gender",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.password === '') {
            ToastAndroid.showWithGravity("Please fill the password",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.password.length < 8) {
            ToastAndroid.showWithGravity("Password length must be greater than 8",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (pass.test(this.state.password) === false) {
            ToastAndroid.showWithGravity("Password must contain at least one uppercase letter, one lowercase letter, one digit and one special character",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass === '') {
            ToastAndroid.showWithGravity("Please fill the confirm password",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass != this.state.password) {
            ToastAndroid.showWithGravity("Password not matched",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.address === '') {
            ToastAndroid.showWithGravity("Please fill the place",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.district === '') {
            ToastAndroid.showWithGravity("Please fill the district",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.stat === '') {
            ToastAndroid.showWithGravity("Please fill the state",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.aadhar_no != '') {
            if (this.state.aadhar_no.replace(/\s|\*|\+|\#/g, '').length !== 12) {
                ToastAndroid.showWithGravity("Please enter a valid aadhar number",ToastAndroid.SHORT,ToastAndroid.CENTER);
                return false;
            } else { return true }
        }
        // else if (this.state.image === '') {
        //     this.setState({ error: "Please select the image" });
        //     return false;
        // }
        else {
            return true;
        }
    }

    onChange = (field, value) => {
        console.log(field, value);
        this.setState({ [field]: value });
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <KeyboardAvoidingView style={[globalStyles.subMainCont, { backgroundColor: colors.white }]}>
                    {/* <ImageBackground style={globalStyles.backgroundImage} source={require('../../../assets/images/login_bg.png')} resizeMode='stretch'> */}
                    <ScrollView>
                        <View style={{ flex: .7 }}>
                            <ProfileComponent
                                state={this.state}
                                prop={this.props}
                                onChange={(field, value) => this.onChange(field, value)} />
                        </View>
                        <View style={{ flex: .3 ,alignItems:'center',justifyContent:'center',paddingVertical:hp('4%')}}>
                           <TouchableOpacity style={[globalStyles.buttonStyle2,{paddingHorizontal:hp('8%'),paddingVertical:hp('1.5%')}]}>
                            <Text style={globalStyles.buttonText}>update</Text>
                           </TouchableOpacity>
                        </View>
                    </ScrollView>
                    {/* </ImageBackground> */}
                </KeyboardAvoidingView>
            </View>
        )

    }
}

const mapStateToProps = state => ({
    user: state.UserReducer.user
});
export default connect(mapStateToProps)(CustomerProfile)