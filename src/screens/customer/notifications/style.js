import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  responsiveBox: {
    width: wp('100%'),
    height: hp('100%'),
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
  },
  flatlistWrap: {
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('3%'),
  },
  itemContainer: {
    borderWidth: 1,
    borderColor: '#eaeaea',
    marginBottom: hp('1.7%'),
    paddingVertical: hp('1.5%'),
    paddingHorizontal: wp('5%'),
    backgroundColor:"white",
    borderRadius: 10,
    elevation: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  txt: {
    color: '#4a697d',
    fontSize: hp('2%'),
  },
  timeWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  time: {
    marginHorizontal: wp('1%'),
    fontSize: hp('1.7%'),
    color: '#96a8b4',
  },
});

export default styles;
