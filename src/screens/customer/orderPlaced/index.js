import React, { Component } from 'react';
import { View, ImageBackground, Image, Text, Alert, Modal, StyleSheet,TouchableOpacity} from 'react-native';
import globalStyles from '../../../globalStyles';
import loginAsStyle from '../../loginAs/loginAsStyle';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import welcomeStyle from '../welcome/welcomeStyle';
import AsyncStorage from '@react-native-community/async-storage';
import colors from '../../../globalStyles/colors';
class OrderPlaced extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false
        };
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <View style={globalStyles.subMainCont}>
                    <ImageBackground style={globalStyles.backgroundImage} source={require('../../../assets/images/bg.png')} resizeMode='stretch'>
                        <View style={globalStyles.mainCont}>
                            <View style={{ flex: .16 }}></View>
                            <View style={{ flex: .38 }}>
                                <View style={loginAsStyle.subMainCon}>
                                    <View style={loginAsStyle.logoMainCon}>
                                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                            <View style={{ height: '62%', width: '21%', alignSelf: 'center' }}>
                                                <Image style={globalStyles.imageStyle} resizeMode='stretch' source={require('../../../assets/images/order_success.png')} />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={welcomeStyle.welcomeMain}>
                                        <View style={globalStyles.view}>
                                            <View style={welcomeStyle.welcomeSub}>
                                                <View style={globalStyles.view}>
                                                    <Text style={loginAsStyle.textStyle}>Successful!</Text>
                                                </View>
                                            </View>
                                            <View style={welcomeStyle.textMain}>
                                                <Text style={welcomeStyle.textStyle}>Your order has been placed{'\n'}
                                                successfully.</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={welcomeStyle.buttonMain}>
                                        <View style={globalStyles.view}>
                                            <TouchableOpacity style={[globalStyles.buttonStyle2, { paddingHorizontal: hp('5%') }]}
                                                onPress={() => {
                                                  this.props.navigation.navigate('MyOrder')
                                                }} >
                                                <Text style={globalStyles.buttonText}>My Orders</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <Modal
                                    transparent={true}
                                    visible={this.state.isVisible}
                                    onRequestClose={()=>this.setState({isVisible:false})}
                                >
                                    <View style={styles.Alert_Main_View}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ flex: .3, flexDirection: 'row' }}>
                                                <View style={{ height: '100%', width: '100%', backgroundColor: colors.primaryColor,
                                                paddingLeft:hp('2%'),justifyContent:'center' }}>
                                                    <Text style={{color:colors.white,fontSize:hp('2.4%'),fontWeight:'bold'}}>LOGOUT</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: .5, flexDirection: 'row', }}>
                                            <View style={{ height: '100%', width: '100%',backgroundColor: colors.white,
                                                paddingLeft:hp('2%'),justifyContent:'center' }}>
                                                    <Text style={{color:colors.black,fontSize:hp('2%')}}>Do you want to logout?</Text>
                                                </View>
                                            </View>
                                            <View style={{ flex: .2, flexDirection: 'column' }}>
                                            <View style={{ flex:1,flexDirection:'row'}}>
                                                    <View style={{flex:.5,backgroundColor:colors.secondColor,marginRight:hp('.1%'),
                                                alignItems:'center',justifyContent:'center'}}>
                                                    <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}}
                                                     onPress={() => { this.setState({isVisible:false}) } }>
                                                        <Text style={{color:colors.white,fontSize:hp('1.8%')}}>Cancel</Text>
                                                    </TouchableOpacity>
                                                    </View>
                                                    <View style={{flex:.5,backgroundColor:colors.secondColor,marginLeft:hp('.1%'),
                                                alignItems:'center',justifyContent:'center'}}>
                                                    <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} 
                                                    onPress={() => { this.setState({isVisible:false})
                                                        AsyncStorage.removeItem('userData');
                                                                    this.props.navigation.navigate('Login') } }>
                                                    <Text style={{color:colors.white,fontSize:hp('1.8%')}}>Ok</Text>
                                                    </TouchableOpacity>
                                                  </View>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                </Modal>
                            </View>
                            <View style={{ flex: .46, }}></View>

                        </View>
                    </ImageBackground>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    Alert_Main_View: {
        alignItems: 'center',
        top: '40%', left: '10%',
        justifyContent: 'center',
        backgroundColor: "#fff",
        height: hp('23%'),
        width: '80%',
        elevation:100
     //  borderWidth: 1,
        // borderColor: colors.primaryColor,
      //  borderRadius: 7,

    },
})
export default OrderPlaced;