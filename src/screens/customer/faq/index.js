import React, {Component} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {List} from 'react-native-paper';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TouchableOpacity, ScrollView} from 'react-native-gesture-handler';
import styles from './style';
import globalStyles from '../../../globalStyles';
import colors from '../../../globalStyles/colors';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';

const question = <Text style={styles.faqQue}>Lorem Ipsum Dolor Sit Amet?</Text>;

class CustomerFAQ extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: colors.primaryColor,
      },
      headerTitle: () => (
        <View style={globalStyles.viewRow}>
          <Text style={globalStyles.headerTitle}>FAQ and Help</Text>
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginLeft: hp('2%')}]}
          onPress={() => navigation.openDrawer('App')}>
          <Icon3 name="menu" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginRight: hp('2%')}]}
          >
          <Icon3 name="cart-outline" size={20} color="#fff" />
        </TouchableOpacity>
      ),
    };
  };

  state = {
    expanded: true,
  };
  _handlePress = () =>
    this.setState({
      expanded: !this.state.expanded,
    });

  render() {
    return (
      <SafeAreaView style={styles.responsiveBox}>
        <ScrollView style={styles.mainContainer}>
          <List.AccordionGroup>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="1">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="2">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="3">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="4">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="5">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="6">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="7">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="8">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="9">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
            <List.Accordion
              style={styles.accordionWrap}
              title={question}
              id="10">
              <View style={styles.faqAnsWrap}>
                <Text style={styles.faqAns}>
                  It is a long established fact that a reader will be distracted
                  by the readable content of a page when looking at its layout.
                  The point of using Lorem Ipsum is that it has a more-or-less
                  normal distribution of letters, as opposed to using 'Content
                  here, content here', making it look like readable English.
                  Many desktop publishing packages and web page editors now use
                  Lorem Ipsum as their default model text, and a search for
                  'lorem ipsum' will uncover many web sites still in their
                  infancy. Various versions have evolved over the years,
                  sometimes by accident, sometimes on purpose (injected humour
                  and the like).
                </Text>
              </View>
            </List.Accordion>
          </List.AccordionGroup>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default CustomerFAQ;
