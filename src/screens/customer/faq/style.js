import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  responsiveBox: {
    width: wp('100%'),
    height: hp('100%'),
  },
  mainContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: wp('1.5%'),
    marginBottom: hp('10%'),
  },
  accordionWrap: {
    borderBottomWidth: wp('0.2%'),
    borderBottomColor: '#e1e1e1',
  },
  faqQue: {
    color: '#525a45',
    fontSize: hp('2%'),
    fontWeight: 'bold',
    textAlign: 'left',
  },
  faqAnsWrap: {
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('1%'),
  },
  faqAns: {
    color: '#525a45',
    fontSize: hp('1.7%'),
    textAlign: 'justify',
    lineHeight: hp('2.8%'),
  },
});

export default styles;
