import React, { Component } from 'react';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import { Image, View, Text, StatusBar, KeyboardAvoidingView, ToastAndroid, TouchableOpacity, Linking, BackHandler } from 'react-native';
import globalStyles from '../../../globalStyles';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../../../globalStyles/colors';
import CreateOrderComponent from '../../../reuseableComponents/createOrderComponent';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import styles from './styles';
import APICaller from '../../../utilities/apiCaller';
import { SearchBar } from 'react-native-elements';
import Loader from '../../../reuseableComponents/loader';
import RNRestart from 'react-native-restart';
let backPressed = 0;
class CreateOrderScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerTitleAlign: 'center',
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>Create Order</Text>
                </View>
            ),
            headerLeft: () => (
                <TouchableOpacity
                    style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                    onPress={() => navigation.openDrawer()}>
                    {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                    <Image style={globalStyles.headerIcon} source={require('../../../assets/images/menu.png')} />
                </TouchableOpacity>
            ),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            shopCategory: ' Category',
            shopName: 'Select Shop',
            catId: '',
            shop_id: '',
            item: [{ item_name: '', weight: '',weight_form:'', quantity: '', notes: '' }],
            pickTime: '',
            notes: '',
            isVisible: false,
            categoryList: [],
            shopList: [],
            shopDetail: '',
            shopkeeperName: 'Shopowner Name',
            // shopContact:'',
            search: '',
            modalValue: '',
            long: '-122.08400000000002',
            lat: '37.421998333333335',
            isLoader:false,
            
        };
        let searchList = [];
        let copySearch = [];
    }
    disableBackButton = () => {
        if(backPressed > 0){
            console.log(backPressed);
            BackHandler.exitApp();
            backPressed = 0;
        }else {
            console.log(backPressed);
            backPressed++;
            ToastAndroid.show("Press again to exit the app", ToastAndroid.SHORT);
            setTimeout( () => { backPressed = 0}, 2500);
            return true;
        }
     
        // BackHandler.exitApp();
        // return true;
      }
      
      componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.disableBackButton);
        this.getAllCategories();
      }
      componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.disableBackButton);
      }
    componentDidMount = () => {
        this.getAllCategories();
    }
    arrayValid = () => {
        let status;
        this.state.item.map((item) => {
            if (item.item_name == '') {
                ToastAndroid.showWithGravity("Please fill the item name", ToastAndroid.SHORT, ToastAndroid.CENTER);
                status=false;
            } else if (item.weight == '' && item.quantity == '') {
                ToastAndroid.showWithGravity("Please fill the weight or quantity for item", ToastAndroid.SHORT, ToastAndroid.CENTER);
                status=false;
            }
            else {
                status=true;
            }
        })
        console.log('STATUS',status);
        return status;
    }

    validateFields = () => {
        let status=this.arrayValid();
        console.log("VHDBSWgbgbJ",status)
        if (this.state.shop_id === '') {
            ToastAndroid.showWithGravity("Please select the shop", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (!status) {
            console.log("VHDBSWJ",status)
            return false;
    
        }else if (this.state.pickTime === '' || this.state.pickTime === undefined) {
            ToastAndroid.showWithGravity("Please select the pickup time for order", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        }
        else {
            return true;
        }
    }
    getAllCategories = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.props.token,
        };
        const endPoint = 'get-categories';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    this.setState({ categoryList: data.data });
                    console.log(this.state.categoryList);
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }

    getShops = async (id) => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.props.token,
        };
        const endPoint = 'categories-shops';
        const body = {
            category_id: id,
            latitude: this.state.lat,
            longitude: this.state.long
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    this.setState({ shopList: data.data });
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }
    categorySelect = () => {
        console.log('States select')
        this.setState({ modalValue: 'category' })
        this.searchList = this.state.categoryList;
        this.copySearch = this.state.categoryList;
        this.toggleModal();
    }
    shopSelect = () => {
        console.log("CAtegory Id", this.state.catId);
        if (this.state.catId != '') {
            this.setState({ modalValue: 'shops' })
            this.searchList = this.state.shopList;
            this.copySearch = this.state.shopList;
            this.toggleModal();
        } else {
            ToastAndroid.showWithGravity("Please select the category", ToastAndroid.SHORT, ToastAndroid.CENTER);
        }

    }
    toggleModal = () => {
        this.setState({ isVisible: !this.state.isVisible });
    };
    onChange = (field, value) => {
        this.setState({ [field]: value });
    }
    onChangeArray = (field, value, i) => {
        let array = [...this.state.item];
        array[i] = { ...array[i], [field]: value };
        this.setState({ item: array });
    }
    getshopDetail = async (id) => {
        console.log(id)
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + this.props.token,
        };
        const endPoint = 'shop-details';
        const body = {
            shop_id: id
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM  SERVER shop detail===>", data);
                if (data.status == 1) {
                    this.setState({ shopDetail: data.data });
                    this.setState({ shopkeeperName: data.data.shopkeeper_details.name })
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }

    itemUpdate = () => {
        let product = this.state.item;
        product.push({ item_name: '' });
        this.setState({ item: product });
    }
    renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1.5,
                    backgroundColor: colors.secondColor,
                    marginHorizontal: '1%',
                }}
            />
        );
    };

    searchFilterFunction = text => {
        this.setState({
            search: text,
        });
        this.searchList = this.copySearch;
        let itemData;
        const newData = this.searchList.filter(item => {
            if(this.state.modalValue == 'category'){
                itemData = `${item.category_name.toUpperCase()}   
                 ${item.category_name.toUpperCase()}`;
        }else{
                itemData = `${item.shop_name.toUpperCase()}   
                ${item.shop_name.toUpperCase()}`;
        }

            const textData = text.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });
        this.searchList = newData;
        console.log("Searchable List", newData);
    };

    selectedValues = (item) => {
        console.log("Selected VALUE FROM FLATLIST=====>", item);
        if (this.state.modalValue == 'category') {
            this.setState({ shopCategory: item.category_name });
            let id = (item.id).toString();
            this.setState({ catId: id });

            this.getShops(item.id);
            this.toggleModal();
        } else {
            this.setState({ shopName: item.shop_name });
            this.setState({ shop_id: item.id });
            this.getshopDetail(item.id);
            this.toggleModal();
        }
    }
    submitOrder = async () =>{
        let status=this.validateFields()
        console.log(status)
        if (status) {
            this.setState({isLoader:true});
            const options = {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.props.token,
            };
            const endPoint = 'create-order';
            const body = {
                shop_id: this.state.shop_id,
                pickup_time: this.state.pickTime,
                notes: this.state.notes,
                items: this.state.item
            };
            console.log('BODY=>>>', body);
            await APICaller(endPoint, 'POST', body, options)
                .then(async response => {
                    const { data } = response;
                    console.log("RESPONSE FROM SERVER===>", data);
                    if (data.status) {
                        this.setState({isLoader:false});
                        this.setState({
                            shopCategory: ' Category',
                            shopName: 'Select Shop',
                            catId: '',
                            shop_id: '',
                            item: [{ item_name: '', weight: '', quantity: '', notes: '' }],
                            pickTime: '',
                            notes: '',
                            shopkeeperName: 'Shopowner Name',
                        })
                        this.props.navigation.navigate('Success');
                        //ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                    }
                    else {
                        alert(data.message);
                    }
                })
                .catch(err => {
                    console.log('Error from server', err);
                });
        }

    }
    renderRow = ({ item, index }) => {
        console.log(item);
        return (<TouchableOpacity
            style={styles.text_input} onPress={() => this.selectedValues(item)}>
            {this.state.modalValue == 'category' ?
                <Text style={styles.text}>{item.category_name}</Text>
                : <Text style={styles.text}>{item.shop_name}</Text>}
        </TouchableOpacity>);
    }
    makeCall=()=>{
        if(this.state.shopDetail != ''){
        console.log("Contact Number",this.state.shopDetail.shopkeeper_details.moile_no)
        const phoneNumber = this.state.shopDetail.shopkeeper_details.moile_no;
        Linking.canOpenURL(`tel:${phoneNumber}`)
          .then(supported => {
            if (!supported) {
                ToastAndroid.showWithGravity('Not a valid phone number', ToastAndroid.SHORT, ToastAndroid.CENTER);
            } else {
              return Linking.openURL(`tel:${phoneNumber}`);
            }
          })
        }
        else{
            ToastAndroid.showWithGravity('Please select a shop', ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <KeyboardAvoidingView style={[globalStyles.subMainCont, { backgroundColor: '#fff', paddingVertical: hp('2%'), paddingHorizontal: hp('2%') }]}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ flex: .7, backgroundColor: '#fff' }}>
                            <CreateOrderComponent
                                state={this.state}
                                onChange={(field, value) => this.onChange(field, value)}
                                onChangeArray={(field, value, i) => this.onChangeArray(field, value, i)}
                                categorySelect={() => this.categorySelect()}
                                itemCreate={() => this.itemUpdate()}
                                shopSelect={() => this.shopSelect()}
                                submitOrder={() => this.submitOrder()}
                                makeCall={()=> this.makeCall()} />

                        </View>
                        <View style={{ flex: .3, backgroundColor: '#fff' }}>
                            {/* <SignUpButton
                            navigation={this.props.navigation}/> */}
                        </View>
                        <Modal
                            // backdropOpacity={0.4}
                            // animationType="fade"
                            backdropColor={colors.white}
                            backdropOpacity={0.55}
                            style={styles.modal}
                            transparent={true}
                            isVisible={this.state.isVisible}
                            onRequestClose={() => this.toggleModal()} // Used to handle the Android Back Button
                        >
                            <View style={{ flex: 1 }}>
                                <SearchBar
                                    containerStyle={{ backgroundColor: colors.primaryColor }}
                                    inputContainerStyle={{ backgroundColor: '#fff' }}
                                    placeholder="Type Here..."
                                    lightTheme
                                    round
                                    value={this.state.search}
                                    onChangeText={text => this.searchFilterFunction(text)}
                                    autoCorrect={false}
                                />
                                <FlatList
                                    data={this.searchList}
                                    renderItem={this.renderRow}
                                    keyExtractor={item => item.id}
                                    ItemSeparatorComponent={this.renderSeparator}
                                />
                            </View>
                        </Modal>
                    </ScrollView>
                </KeyboardAvoidingView>
                <Loader isLoader={this.state.isLoader}></Loader>
            </View>
        );

    }
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
    token: state.TokenReducer.token
});
export default connect(mapStateToProps)(CreateOrderScreen);