import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    text_input: {
        paddingHorizontal: hp('1%'),
        paddingVertical: hp('1%'),
      },
      text: {
        fontSize: hp('2%'),
        fontFamily: "Serif-Bold",
        color: "#000",
      },    
    modal: {
    margin: 0, 
    backgroundColor: 'white', 
    // top: '30%',
    // left: '5%',
    height: hp('40%'), 
    flex:0, 
  bottom: 0,
    position: 'absolute',
    width: '100%', }
});

export default styles;