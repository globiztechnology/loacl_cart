import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
   inputMain:{
    height: hp('7%'),
    paddingHorizontal: hp('2%'),
    paddingVertical: hp('1%')
   }
});