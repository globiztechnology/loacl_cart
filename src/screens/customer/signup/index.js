import React, { Component } from 'react';
import globalStyles from "../../../globalStyles"
import { KeyboardAvoidingView, ImageBackground, View, Text, Image, StatusBar, Alert, ToastAndroid } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import colors from '../../../globalStyles/colors';
import SignUpComponent from '../../../reuseableComponents/signupComponent';
import SignUpButton from '../../../reuseableComponents/signupButton';
import APICaller from '../../../utilities/apiCaller';
import ImagePicker from 'react-native-image-picker';

const options = {
    title: 'Select a Photo',
    takePhotoButtonTitle: 'Take a Photo',
    chooseFromLibraryButtonTitle: 'Choose from Library',
    quality: 1,
};


class CustomerSignUp extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>Signup</Text>
                </View>
            ),
            headerLeft: () => (
                <TouchableOpacity
                    style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                    onPress={() => navigation.goBack()}>
                    {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                    <Image style={globalStyles.headerIcon} source={require('../../../assets/images/back.png')} />
                </TouchableOpacity>
            ),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            name: '',
            email: '',
            password: '',
            mobile: '',
            address: '',
            aadhar_no: '',
            // user_type:'',
            profile_pic:'',
            gender: '',
            district: '',
            stat: '',
            confirmPass: '',
            image: '',
            defaultImage: '',
            states:[],
            cities:[],
        };
    }
    componentDidMount = () => {
        this.getAllCity();
        this.getAllStates();
    }
    validateFields = () => {
        console.log("Gender", this.state.gender);
        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var pass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[^\w\d]).*$/;
        if (this.state.name === '') {
            ToastAndroid.showWithGravity("Please fill the name",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile === '') {
            ToastAndroid.showWithGravity("Please fill the mobile number",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile.replace(/\s|\*|\+|\#/g, '').length !== 10) {
            ToastAndroid.showWithGravity("Please enter a valid mobile number",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.email === '') {
            ToastAndroid.showWithGravity("Please fill the email",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (email.test(this.state.email) === false) {
            ToastAndroid.showWithGravity("Email is not correct",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.gender === '' || this.state.gender === undefined) {
            ToastAndroid.showWithGravity("Please select the gender",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.password === '') {
            ToastAndroid.showWithGravity("Please fill the password",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.password.length < 8) {
            ToastAndroid.showWithGravity("Password length must be greater than 8",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (pass.test(this.state.password) === false) {
            ToastAndroid.showWithGravity("Password must contain at least one uppercase letter, one lowercase letter, one digit and one special character",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass === '') {
            ToastAndroid.showWithGravity("Please fill the confirm password",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass != this.state.password) {
            ToastAndroid.showWithGravity("Password not matched",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.address === '') {
            ToastAndroid.showWithGravity("Please fill the place",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        } else if (this.state.district === '' || this.state.district === undefined) {
            ToastAndroid.showWithGravity("Please fill the district",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.stat === '' || this.state.stat === undefined) {
            ToastAndroid.showWithGravity("Please fill the state",ToastAndroid.SHORT,ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.aadhar_no != '') {
            if (this.state.aadhar_no.replace(/\s|\*|\+|\#/g, '').length !== 12) {
                ToastAndroid.showWithGravity("Please enter a valid aadhar number",ToastAndroid.SHORT,ToastAndroid.CENTER);
                return false;
            } else { return true }
        }
        // else if (this.state.image === '') {
        //     this.setState({ error: "Please select the image" });
        //     return false;
        // }
        else {
            return true;
        }
    }
    getAllCity = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'get-cities/32';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                    this.setState({ cities: data.data });
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }
    getAllStates = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'get-states';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                    this.setState({ states: data.data });
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }
    _uploadImage = async (image) => {

        console.log('Uploading Profile Image');
        const endPoint = 'image-upload';
        const body = {
            type: "user_profile",
            image: 'data:image/jpeg;base64,' + image.data
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body).then(response => {
            const { data } = response;
            console.log('DATA----------->', data);
            if (data.status) {
              //  this.setState({image: data.data.url})
               // console.log(this.state.image);
                console.log(data.message);
                this.setState({profile_pic:data.data.image_name});
                ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
            } else {
                console.log(data.message);
            }
        })
            .catch((error) => {
                console.error(error);
            });
    }

    _postUser = async () => {
        console.log('USER SIGNUP INFORMATION');
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'register';

        const body = {
             profile_pic: this.state.profile_pic,
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            mobile_no: this.state.mobile,
            address: this.state.address,
            user_type: 'customer',
            gender: this.state.gender,
            district: this.state.district,
            state: this.state.stat,
            aadhar_no: this.state.aadhar_no,
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                    this.props.navigation.navigate('Login');
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    };
    handleSignUp = async () => {
        const status = await this.validateFields();
        console.log(status)
        if (status) {
            this._postUser();
        } 
        // else {
        //     Alert.alert("Error", this.state.error);
        // }
    }
    selectImage = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                this._uploadImage(response);
                this.setState({image:response})
            }
        });

    }

    onChange = (field, value) => {
        console.log(field, value);
        this.setState({ [field]: value });
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <KeyboardAvoidingView style={[globalStyles.subMainCont, { backgroundColor: colors.white }]}>
                    {/* <ImageBackground style={globalStyles.backgroundImage} source={require('../../../assets/images/login_bg.png')} resizeMode='stretch'> */}
                    <ScrollView>
                        <View style={{ flex: .7 }}>
                            <SignUpComponent
                                state={this.state}
                                selectImage={() => this.selectImage()}
                                onChange={(field, value) => this.onChange(field, value)} />
                        </View>
                        <View style={{ flex: .3 }}>
                            <SignUpButton
                                navigation={this.props.navigation}
                                handleSignUp={() => this.handleSignUp()} />
                        </View>
                    </ScrollView>
                    {/* </ImageBackground> */}
                </KeyboardAvoidingView>
            </View>
        )

    }
}

export default CustomerSignUp