import React, { Component } from 'react';
import globalStyles from '../../../globalStyles';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import OrderListComponent from '../../../reuseableComponents/orderList';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../../../globalStyles/colors';
import { Image, View, Text, StatusBar, Modal, TouchableOpacity, StyleSheet } from 'react-native';
import styles from './styles';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';
import { format } from "date-fns";
import { connect } from 'react-redux';
import APICaller from '../../../utilities/apiCaller';
import Loader from '../../../reuseableComponents/loader';

class MyOrders extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitleAlign: 'center',
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>My Orders</Text>
                </View>
            ),
            headerLeft: () => (
                <TouchableOpacity
                    style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                    onPress={() => navigation.openDrawer()}>
                    {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                    <Image style={globalStyles.headerIcon} source={require('../../../assets/images/menu.png')} />
                </TouchableOpacity>

            ),
            headerRight: () => (
                <TouchableOpacity style={[globalStyles.viewRow, { marginRight: hp('2%') }]}>
                    <Image style={globalStyles.headerRightIcon} source={require('../../../assets/images/cart.png')} />
                </TouchableOpacity>
            ),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            orders: [],
            isLoader: false
        };
    }
    componentDidMount() {

        this.orderList()

    }
    componentDidUpdate() {
        this.props.navigation.addListener('willFocus', () => {
            this.orderList()
        })
    }
    detailOrder = () => {
        this.setState({ visible: !this.state.visible });
    }

    orderList = async () => {
        this.setState({ isLoader: true })
        console.log('ORDER LISTING');
        const options = {
            Authorization: 'Bearer ' + this.props.token,
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'my-orders';

        const body = {
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                    this.setState({ orders: data.data });
                }
                else {

                    //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
        this.setState({ isLoader: false })
    }
    status = (item) => {
        if (item.status == 'completed') {
            return (
                <View style={[button.button, { backgroundColor: colors.secondColor }]}>
                    <Text style={globalStyles.buttonText}>{item.status}</Text>
                </View>
            )
        } else if (item.status == 'pending') {
            return (
                <View style={[button.button, { backgroundColor: colors.primaryColor }]}>
                    <Text style={globalStyles.buttonText}>{item.status}</Text>
                </View>
            )
        } else if (item.status == 'accepted') {
            return (
                <View style={[button.button, { backgroundColor: 'green' }]}>
                    <Text style={globalStyles.buttonText}>{item.status}</Text>
                </View>
            )
        } else if (item.status == 'prepared') {
            return (
                <View style={[button.button, { backgroundColor: '#e95677' }]}>
                    <Text style={globalStyles.buttonText}>{item.status}</Text>
                </View>
            )
        } else {
            return (
                <View style={[button.button, { backgroundColor: 'red' }]}>
                    <Text style={globalStyles.buttonText}>{item.status}</Text>
                </View>
            )
        }
    }
    render() {
        let date = new Date("2016-01-04T10:34:23");

        let formattedDate = format(date, "dd-MMM-yyyy");
        let time = format(date, "hh:mm a");
        console.log(formattedDate, time);
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <View style={globalStyles.subMainCont}>
                    {this.state.orders
                        ? <View style={{ flex: 1 }}>
                            <FlatList
                                style={{ flex: 1, padding: hp('1.5%') }}
                                data={this.state.orders}
                                renderItem={({ item }) => <OrderListComponent item={item} status={(item) => this.status(item)}
                                    detailOrder={() => this.detailOrder()} />}
                            //  keyExtractor={item => item.id}
                            />
                            <View style={{ position: 'absolute', zIndex: 100, bottom: hp('6%'), height: hp('6%'), left: 0, right: 0 }}>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity style={{
                                        width: hp('25%'), height: hp('5.5%'), marginLeft: hp('1%'), alignItems: 'center',
                                        justifyContent: 'center', backgroundColor: colors.secondColor, borderRadius: hp('2%'), opacity: .9
                                    }}
                                        onPress={() =>this.props.navigation.navigate('CreateOrder') }>
                                        <Text style={globalStyles.buttonText}>Create Order</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: hp('2%') }}>No Order Avaliable</Text></View>
                    }
                    <Modal
                        transparent={true}
                        animationType={'slide'}
                        visible={this.state.visible}>
                        <ScrollView>
                            <View style={styles.mdlBackground}>
                                <View style={styles.mdlWrap}>
                                    <View>
                                        <View style={styles.toggleIcn}>
                                            <TouchableOpacity
                                                onPress={() => this.detailOrder()}
                                                style={styles.mdlArrwWrap}>
                                                <Icon
                                                    name="angle-down"
                                                    size={35}
                                                    color="#fff"
                                                    style={styles.mdlArrw}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ height: hp('11%') }}>
                                        <View
                                            style={{
                                                flex: 1,
                                                flexDirection: 'row',
                                                paddingHorizontal: wp('4%'),
                                            }}>
                                            <View
                                                style={{
                                                    flex: 0.8,
                                                    alignItems: 'flex-start',
                                                    justifyContent: 'center',
                                                    paddingVertical: hp('2'),
                                                }}>
                                                <View style={{ flex: 1 }}>
                                                    <View style={{ flex: 0.7 }}>
                                                        <View style={styles.cstmrNameWrap}>
                                                            <Text style={styles.srchShopTxt}>
                                                                Order Id: #14753
                                </Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ flex: 0.3 }}>
                                                        <View style={styles.ordrTimeWrap}>
                                                            <Icon2 name="clock" size={15} color="#e97657" />
                                                            <Text style={styles.ordrTime}>
                                                                28 April | 04:00 PM
                                </Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                            <View
                                                style={{
                                                    flex: 0.2,
                                                    alignItems: 'flex-end',
                                                    justifyContent: 'center',
                                                    paddingBottom: hp('2%'),
                                                }}>
                                                <TouchableOpacity style={styles.phnIcnWrap}>
                                                    <Icon2
                                                        name="phone"
                                                        size={24}
                                                        color="#fff"
                                                        style={styles.phoneIcn}
                                                    />
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.mdlCrd}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ flex: 0.2 }}>
                                                <View
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'row',
                                                        borderBottomWidth: 1,
                                                        marginBottom: hp('1%'),
                                                        borderBottomColor: '#c7c7c7',
                                                    }}>
                                                    <View style={{ flex: 0.8 }}>
                                                        <Text style={{ fontSize: hp('2') }}>
                                                            Item Name Here
                              </Text>
                                                    </View>
                                                    <View style={{ flex: 0.2 }}>
                                                        <Text
                                                            style={{
                                                                fontSize: hp('1.7%'),
                                                                textAlign: 'right',
                                                            }}>
                                                            1 KG
                              </Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={{ flex: 0.6 }}>
                                                <View style={{ flex: 1 }}>
                                                    <Text
                                                        style={{
                                                            height: hp('3.2%'),
                                                            fontSize: hp('2%'),
                                                            color: '#515943',
                                                        }}>
                                                        Note: {'\n'}{' '}
                                                    </Text>
                                                    <Text
                                                        style={{
                                                            color: '#919489',
                                                            fontSize: hp('1.5%'),
                                                            lineHeight: hp('2.3%'),
                                                        }}>
                                                        Lorem Ipsum is simply dummy text of the printing
                                                        and typesetting industry. Lorem Ipsum has been
                            </Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.mdlCrd}>
                                        <View style={{ flex: 1 }}>
                                            <Text
                                                style={{
                                                    color: '#515943',
                                                    fontSize: hp('2%'),
                                                    marginBottom: hp('0.4%'),
                                                }}>
                                                Customer Comments:{' '}
                                            </Text>
                                            <Text
                                                style={{
                                                    color: '#919489',
                                                    fontSize: hp('1.5%'),
                                                    lineHeight: hp('2.3%'),
                                                }}>
                                                Lorem Ipsum is simply dummy text of the printing and
                                                typesetting industry. Lorem Ipsum has been the
                                                industry
                        </Text>
                                        </View>
                                    </View>
                                    <View style={{ height: hp('15') }}>
                                        <View style={{ flex: 1, alignItems: 'center' }}>
                                            <View style={styles.btnContainer}>
                                                <TouchableOpacity style={styles.btn}>
                                                    <Text style={styles.btnTxt}>REORDER</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </Modal>
                    <Loader isLoader={this.state.isLoader}></Loader>
                </View>

            </View>
        )
    }
}

const mapStateToProps = state => ({
    user: state.UserReducer.user,
    token: state.TokenReducer.token
});
const button = StyleSheet.create({
    button: {
        top: 0, right: 0, height: hp('4.5%'), alignItems: 'center', justifyContent: 'center'
    }
});
export default connect(mapStateToProps)(MyOrders);