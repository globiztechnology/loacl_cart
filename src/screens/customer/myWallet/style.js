import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  responsiveBox: {
    width: wp('100%'),
    height: hp('100%'),
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
  },
  wltInfoSec: {
    flex: 0.24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  blncTtl: {
    color: '#515945',
    fontSize: hp('2.2%'),
  },
  blncWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: hp('1.5%'),
  },
  blncIcnWrap: {
    width: hp('3.5%'),
    height: hp('3.5%'),
    borderRadius: hp('1.75%'),
    backgroundColor: '#beba59',
    paddingVertical: hp('1%'),
  },
  blncIcn: {
    textAlign: 'center',
  },
  blncAmt: {
    color: '#515945',
    fontSize: hp('4.5%'),
    fontWeight: 'bold',
    marginHorizontal: wp('3%'),
  },
  adMnyBtn: {
    backgroundColor: '#e97657',
    paddingVertical: wp('3.7%'),
    paddingHorizontal: hp('4%'),
    borderRadius: 30,
  },
  btnTxt: {
    color: '#fff',
    fontSize: hp('1.7%'),
    textAlign: 'center',
    letterSpacing: wp('0.5%'),
    textTransform: 'uppercase',
  },
  wltAdMnySec: {
    flex: 0.12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  adMnyWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 10,
    borderWidth: 1,
    width: wp('90%'),
    shadowRadius: 5,
    borderRadius: 10,
    shadowOpacity: 0.5,
    shadowColor: '#000',
    borderColor: '#eaeaea',
    backgroundColor: 'white',
    shadowOffset: {width: 0, height: 3},
  },
  adMnyInputWrap: {
    width: wp('60%'),
    height: hp('5.6%'),
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f7f7f7',
    borderWidth: 1,
    borderColor: '#f7f7f7',
    borderRadius: 30,
    marginVertical: hp('2%'),
    marginLeft: wp('4%'),
    paddingHorizontal: wp('5%'),
  },
  adMnyInput: {
    paddingLeft: wp('3%'),
    fontSize: hp('2.2%'),
  },
  payBtn: {
    height: hp('5.6%'),
    width: wp('20%'),
    borderRadius: 30,
    marginLeft: wp('2%'),
    paddingVertical: hp('1.5%'),
    backgroundColor: '#beba59',
  },
  wltTrnsctnsSec: {
    flex: 0.64,
    alignItems: 'center',
  },
  wltTrnsctnsWrap: {
    borderWidth: 1,
    width: wp('90%'),
    height: hp('50%'),
    marginVertical: hp('2%'),
    borderColor: '#d6d6d6',
  },
  wltTrnsctnsHedWrap: {
    backgroundColor: '#515945',
    paddingVertical: hp('2%'),
  },
  wltTrnsctnsHed: {
    color: '#fff',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontSize: hp('2.2%'),
    fontWeight: 'bold',
    letterSpacing: wp('0.6%'),
  },
  wltTrnsctnsListWrap: {
    height: hp('44%'),
    paddingVertical: hp('2%'),
    paddingHorizontal: wp('3%'),
    backgroundColor: '#f7f7f7',
  },
  trnsctnWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  trnsctnDtlWrap: {
    flex: 0.75,
  },
  trnsctnParty: {
    fontSize: hp('2%'),
    color: '#515945',
    marginBottom: hp('0.5%'),
  },
  trnsctnDtTm: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  trnsctnDt: {
    color: '#515945',
    fontSize: hp('1.3%'),
  },
  DtTmSprtr: {
    color: '#515945',
    fontSize: hp('1.3%'),
    marginHorizontal: wp('1.3%'),
  },
  trnsctnTm: {
    color: '#515945',
    fontSize: hp('1.3%'),
  },
  trnsctnAmtWrap: {
    flex: 0.25,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  trnsctnInOut: {
    color: '#beba59',
    fontSize: hp('2.4%'),
  },
  trnsctnCrncy: {
    color: '#beba59',
    fontSize: hp('2.4%'),
    marginHorizontal: wp('1.3%'),    
  },
  trnsctnAmt: {
    color: '#beba59',    
    fontSize: hp('2%'),
  },
  listSeparator: {
    height: hp('0.2%'),
    marginVertical: hp('1.5%'),
    backgroundColor: '#e3e3e3',
  },
});

export default styles;
