import React, { Component } from 'react';

import { View, ScrollView, Alert, ImageBackground, Text, Image,TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
//import {mailIcon} from '../../assets/images/mail.png';
import userIcon from '../../assets/images/profile.png';
import cartIcon from '../../assets/images/menu_cart.png';
import faqIcon from '../../assets/images/faq.png';
import walletIcon from '../../assets/images/money.png';
import privacyIcon from '../../assets/images/privacy.png';
import termIcon from '../../assets/images/terms.png';
import logoutIcon from '../../assets/images/logout.png';
import telIcon from '../../assets/images/tel.png';
import rewardIcon from '../../assets/images/dollor.png';
import refferIcon from '../../assets/images/add.png';
import { DrawerListItem } from '../drawer/drawerRow';
import AsyncStorage from '@react-native-community/async-storage';
import { Linking } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { connect } from 'react-redux';
import colors from '../../globalStyles/colors';
import globalStyles from '../../globalStyles';
class DrawerScreen extends Component {


    render() {
        return (

            <View style={{ flex: 1 }}>
                {/* <View style={{ flex: .3 }}> */}

                <View style={{ height: hp('17%'), width: '100%', backgroundColor: colors.primaryColor }}>

                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <View style={{ height: hp('17%'), justifyContent: 'center' }}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                {/* <TouchableOpacity style={{
                                    height: hp('5%'),
                                    width: hp('5%'),
                                    position: 'absolute',
                                    zindex: 100,
                                    top: hp('7%'),
                                    right: 0,
                                    overflow: 'hidden',
                                    borderRadius: hp('2.5%'),
                                    backgroundColor: colors.secondColor
                                }}>
                                    <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }} >
                                        <Icon name='chevron-left' color='#fff' size={30} />
                                    </TouchableOpacity>
                                </TouchableOpacity> */}
                                <View style={{ flex: .35, alignItems: 'center', }}>
                                    <View style={{
                                        height: hp('10%'),
                                        width: hp('10%'),
                                        borderRadius: hp('5%'),
                                        borderWidth: hp('.2%'),
                                        borderColor: '#fff',
                                        overflow: 'hidden',
                                    }}>
                                        <Image style={{ height: '100%', width: '100%' }} source={require('../../assets/images/girl.png')} />
                                    </View>
                                    <View style={{
                                        height: hp('3%'),
                                        width: hp('3%'),
                                        position: 'absolute',
                                        zindex: 100,
                                        bottom: 0,
                                        left: hp('9.5%'),
                                        overflow: 'hidden',
                                        borderRadius: hp('1.5%'),
                                        backgroundColor: '#fff'
                                    }}>
                                        <TouchableOpacity style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                            <Icon name='camera' color='#e97657' size={15} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{ flex: .65, }}>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <View style={{ height: hp('5%') }}>
                                            <Text numberOfLines={1} adjustsFontSizeToFit={true} style={{ fontSize: hp('2.5%'), color: '#fff', fontWeight: 'bold' }}>{this.props.user.name}</Text>
                                        </View>
                                        <View style={{ height: hp('6%') }}>
                                            <View style={{ flex: 1 }}>
                                                <Text numberOfLines={1} adjustsFontSizeToFit={true} style={{ fontSize: hp('1.7%'), color: '#fff' }}>{this.props.user.email}</Text>
                                            </View>
                                            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                                <Text numberOfLines={1} adjustsFontSizeToFit={true} style={{ fontSize: hp('1.7%'), color: '#fff' }}>{this.props.user.mobile_no}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                {/* </View>
                    <View style={{ flex: .7 }}> */}
                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                    <DrawerListItem
                        iconImage={userIcon}
                        iconHeight={hp('2.8%')}
                        iconWidth={hp('2.8%')}
                        itemName={'Profile'}
                        onPress={() => {
                             this.props.navigation.navigate('CustProfile');
                        }}
                    />
                    <DrawerListItem
                        iconImage={cartIcon}
                        iconHeight={hp('2.8%')}
                        iconWidth={hp('2.8%')}
                        itemName={'Orders'}
                        onPress={() => {
                            this.props.navigation.navigate('MyOrder');
                        }}
                    />
                    <DrawerListItem
                        iconImage={walletIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Wallet'}
                        onPress={() => {
                           // alert('Working')
                             this.props.navigation.navigate('CustWallet');
                        }}
                    />
                    <DrawerListItem
                        iconImage={refferIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Referral'}
                        onPress={() => {
                            alert('Working')
                        }}
                    />
                    <DrawerListItem
                        iconImage={rewardIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Reward Points'}
                        onPress={() => {
                            alert('Working')
                        }}
                    />
                    <DrawerListItem
                        iconImage={faqIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'FAQ'}
                        onPress={() => {
                            // Linking.openURL("mailto:?to=r.sterk@spotter-search.co.uk");
                            this.props.navigation.navigate('CustFaq');
                        }}
                    />
                    <DrawerListItem
                        iconImage={telIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Contact Us/Feedback'}
                        onPress={() => {
                            //this.props.navigation.navigate('help');
                          //  alert('Working')
                        }}
                    />
                    <DrawerListItem
                        iconImage={faqIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'About Us'}
                        onPress={() => {
                            this.props.navigation.navigate('CustAbout');
                        }}
                    />
                    <DrawerListItem
                        iconImage={privacyIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Privacy Policy'}
                        onPress={() => {
                            this.props.navigation.navigate('CustPrivacy');
                        }}
                    />
                    <DrawerListItem
                        iconImage={termIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Terms & Conditions'}
                        onPress={() => {
                            this.props.navigation.navigate('CustTerm');
                        }}
                    />
                    <DrawerListItem
                        iconImage={logoutIcon}
                        iconHeight={hp('3%')}
                        iconWidth={hp('3%')}
                        itemName={'Logout'}
                        onPress={() => {

                            Alert.alert(
                                'Logout',
                                'Do you want to logout?',
                                [
                                    { text: 'Cancel', onPress: () => { return null },style: 'destructive'},
                                    {
                                        text: 'Ok', onPress: () => {
                                            AsyncStorage.removeItem('userData');
                                            AsyncStorage.removeItem('token');
                                            this.props.navigation.navigate('Login')
                                        }
                                    },
                                ],
                                { cancelable: false }
                            )

                        }}
                    />

                    <View style={{ height: hp('15%'), width: '100%', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ paddingVertical: hp('1.5%'), width: '86%', borderRadius: hp('3%'), alignSelf: 'center', 
                        backgroundColor: colors.secondColor }}
                        onPress={()=>this.props.navigation.navigate('CreateOrder')}>
                            <Text style={{
                                fontSize: hp('2.5%'),
                                letterSpacing: 2,
                                textAlign: 'center',
                                color: colors.white,
                                textTransform: 'uppercase'
                            }}>Create Order</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View >
        );
    }
}
const mapStateToProps = state => ({
    user: state.UserReducer.user
});

export default connect(mapStateToProps)(DrawerScreen);
