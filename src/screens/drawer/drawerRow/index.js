import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const DrawerListItem = props => {
  return (
    <TouchableOpacity
      style={{
        flex: 1,
        flexDirection: 'row',
        paddingRight: 20,
        marginTop: hp('4%'),
        justifyContent: 'center',
        alignItems: 'center',
        // borderTopColor: '#ddd',
        // borderTopWidth: 1,
      }}
       onPress={() => props.onPress()}
     >
      <View style={{flex: 0.2,alignItems:'center',justifyContent:'center'}}>
        <Image
          source={props.iconImage}
          style={{height: props.iconHeight, width: props.iconWidth}}
        />
      </View>
      <View style={{flex: 0.8}}>
        <Text
          style={{
            fontSize: hp('2%'),
            color: '#000',
            // fontWeight:'bold'
          }}>
          {props.itemName}
        </Text>
      </View>
    </TouchableOpacity>
  );
};