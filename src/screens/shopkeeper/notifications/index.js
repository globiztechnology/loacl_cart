import React, {Component, useState} from 'react';
import {View, Text, FlatList, SafeAreaView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TouchableOpacity} from 'react-native-gesture-handler';
import styles from './style';
import colors from '../../../globalStyles/colors';
import globalStyles from '../../../globalStyles';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';
import Icon5 from 'react-native-vector-icons/Ionicons';

class Notifications extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: colors.primaryColor,
      },
      headerTitle: () => (
        <View style={globalStyles.viewRow}>
          <Text style={globalStyles.headerTitle3}>Notifications</Text>
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginLeft: hp('2%')}]}
          onPress={() => navigation.goBack()}>
          <Icon3 name="menu" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginRight: hp('2%')}]}
          onPress={() => navigation.navigate('AboutScreen')}>
          <Icon3 name="cart-outline" size={20} color="#fff" />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      Data: [
        {
          id: 1,
          msg: 'You Order has been packed.',
          time: '2 min ago',
        },
        {
          id: 2,
          msg: 'You Order has been packed.',
          time: '2 min ago',
        },
        {
          id: 3,
          msg: 'You Order has been packed.',
          time: '2 min ago',
        },
        {
          id: 4,
          msg: 'You Order has been packed.',
          time: '2 min ago',
        },
        {
          id: 5,
          msg: 'You Order has been packed.',
          time: '2 min ago',
        },
      ],
    };
  }
  render() {
    return (
      <SafeAreaView style={styles.responsiveBox}>
        <View style={styles.body}>
          <FlatList
            style={styles.flatlistWrap}
            data={this.state.Data}
            renderItem={({item}) => (
              <View style={styles.itemContainer}>
                <View style={styles.txtWrap}>
                  <Text style={styles.txt}>{item.msg}</Text>
                </View>
                <View style={styles.timeWrap}>
                  <Icon5 name="md-alarm" size={15} color="#bdb85c" />
                  <Text style={styles.time}>{item.time}</Text>
                </View>
              </View>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </SafeAreaView>
    );
  }
}

export default Notifications;
