import React, { Component } from 'react';
import globalStyles from "../../../globalStyles"
import { KeyboardAvoidingView, ImageBackground, View, Text, Image, StatusBar, PermissionsAndroid, ToastAndroid, Alert } from "react-native"
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import colors from '../../../globalStyles/colors';
import SignUpComponent from '../../../reuseableComponents/signupComponent';
import SignUpButton from '../../../reuseableComponents/signupButton';
import ShopSignupComp from '../../../reuseableComponents/signupComponent/ShopSignupComp';
import Geolocation from '@react-native-community/geolocation';
//import Geolocation from 'react-native-geolocation-service';
import ImagePicker from 'react-native-image-picker';
import APICaller from '../../../utilities/apiCaller';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { setUserData } from '../../../redux/actions/user';
import DateTimePicker from '@react-native-community/datetimepicker';
import { format } from "date-fns";

const options = {
    title: 'Select a Photo',
    takePhotoButtonTitle: 'Take a Photo',
    chooseFromLibraryButtonTitle: 'Choose from Library',
    quality: 1,
};

class ShopSignUp extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>Signup</Text>
                </View>
            ),
            headerLeft: () => (
                <TouchableOpacity
                    style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                    onPress={() => navigation.goBack()}>
                    {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                    <Image style={globalStyles.headerIcon} source={require('../../../assets/images/back.png')} />
                </TouchableOpacity>
            ),
        };
    };
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            name: '',
            email: '',
            password: '',
            mobile: '',
            address: '',
            aadhar_no: '',
            gender: '',
            district: '',
            stat: '',
            confirmPass: '',
            image: '',
            profile_pic: '',
            defaultImage: '',
            category_id: '',
            category: [],
            shop_open_time:'Select Open Time',
            shop_close_time:'Select Close Time',
            bank_name: '',
            account_no: '',
            confirm_account: '',
            shop_name: '',
            cities:[],
            states:[],
            long: '-122.08400000000002',
            lat: '37.421998333333335',
            time:false,
            close:false
        };
    }
    componentDidMount = () => {
        this.getAllCategories();
        this.getAllCity();
        this.getAllStates();
    }
    showDatePicker = () => {
        this.setState({time:true})
      };
     hideDatePicker = () => {
        this.setState({time:false})
      };
    
      handleConfirm = (date) => {
        this.hideDatePicker();
        let time = format(date, "hh:mm a");
        this.setState({shop_open_time:time});
        
      };
      showDatePicker1 = () => {
        this.setState({close:true})
      };
     hideDatePicker1 = () => {
        this.setState({close:false})
      };
    
      handleShopClose = (date) => {
        this.hideDatePicker1();
        let time1 = format(date, "hh:mm a");
        this.setState({shop_close_time:time1})
        
      };

      getAllCity = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'get-cities/32';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                    this.setState({ cities: data.data });
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }
    getAllStates = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'get-states';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                    this.setState({ states: data.data });
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }
    getAllCategories = async () => {
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'get-categories';
        const body = '';
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    this.setState({ category: data.data });
                    console.log(this.state.category);
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    }

    validateFields = () => {
        console.log("Gender", this.state.gender);
        var email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        var pass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[^\w\d]).*$/;
        if (this.state.lat === '' || this.state.long === '') {
            Alert.alert("Error", "Please turn on the location");
            // this.requestLocationPermission();
            return false;
        }
        else if (this.state.name === '') {
            ToastAndroid.showWithGravity("Please fill the name", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile === '') {
            ToastAndroid.showWithGravity("Please fill the mobile number", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.mobile.replace(/\s|\*|\+|\#/g, '').length !== 10) {
            ToastAndroid.showWithGravity("Please enter a valid mobile number", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.email === '') {
            ToastAndroid.showWithGravity("Please fill the email", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (email.test(this.state.email) === false) {
            ToastAndroid.showWithGravity("Email is not correct", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.gender === '' || this.state.gender === undefined) {
            ToastAndroid.showWithGravity("Please select the gender", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.password === '') {
            ToastAndroid.showWithGravity("Please fill the password", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.password.length < 8) {
            ToastAndroid.showWithGravity("Password length must be greater than 8", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (pass.test(this.state.password) === false) {
            ToastAndroid.showWithGravity("Password must contain at least one uppercase letter, one lowercase letter, one digit and one special character", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass === '') {
            ToastAndroid.showWithGravity("Please fill the confirm password", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirmPass != this.state.password) {
            ToastAndroid.showWithGravity("Password not matched", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.address === '') {
            ToastAndroid.showWithGravity("Please fill the place", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.district === '' || this.state.district === undefined) {
            ToastAndroid.showWithGravity("Please fill the district", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        }
        else if (this.state.stat === '' || this.state.stat === undefined) {
            ToastAndroid.showWithGravity("Please fill the state", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        }else if (this.state.category_id === '' || this.state.category_id === undefined) {
            ToastAndroid.showWithGravity("Please select the shop category", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.shop_name === '') {
            ToastAndroid.showWithGravity("Please fill the shop name", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.shop_open_time === '') {
            ToastAndroid.showWithGravity("Please fill the shop opening time", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.shop_close_time === '') {
            ToastAndroid.showWithGravity("Please fill the shop close time", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.bank_name === '') {
            ToastAndroid.showWithGravity("Please fill the bank name", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.account_no === '') {
            ToastAndroid.showWithGravity("Please fill the account number", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirm_account === '') {
            ToastAndroid.showWithGravity("Please fill the confirm account number", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        } else if (this.state.confirm_account != this.state.account_no) {
            ToastAndroid.showWithGravity("Bank account not matched", ToastAndroid.SHORT, ToastAndroid.CENTER);
            return false;
        }else if (this.state.aadhar_no != '') {
            if (this.state.aadhar_no.replace(/\s|\*|\+|\#/g, '').length !== 12) {
                ToastAndroid.showWithGravity("Please enter a valid aadhar number", ToastAndroid.SHORT, ToastAndroid.CENTER);
                return false;
            }else{
                return true;
            }
        }
        // else if (this.state.image === '') {
        //     this.setState({ error: "Please select the image" });
        //     return false;
        // }
        else {
            return true;
        }
    }
    _postUser = async () => {
        console.log('USER SIGNUP INFORMATION');
        const options = {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
        };
        const endPoint = 'shopkeeper-register';

        const body = {
            profile_pic: this.state.profile_pic,
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            mobile_no: this.state.mobile,
            address: this.state.address,
            user_type: 'shopkeeper',
            gender: this.state.gender,
            district: this.state.district,
            state: this.state.stat,
            aadhar_no: this.state.aadhar_no,
            category_id: this.state.category_id,
            shop_name: this.state.shop_name,
            shop_open_time: this.state.shop_open_time,
            shop_close_time: this.state.shop_close_time,
            bank_name: this.state.bank_name,
            account_no: this.state.account_no,
            latitude: this.state.lat,
            longitude: this.state.long
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body, options)
            .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status == 1) {
                    // console.log(data.user_details);
                    // await AsyncStorage.setItem(
                    //     'userData',
                    //     JSON.stringify(data.user_details),
                    // );
                    // this.props.dispatch(setUserData(data.user_details));
                    ToastAndroid.showWithGravity(data.success, ToastAndroid.SHORT, ToastAndroid.CENTER);
                    this.props.navigation.navigate('Login');
                }
                else {
                    alert(data.message);
                }
            })
            .catch(err => {
                console.log('Error from server', err);
            });
    };
    handleSignUp = async () => {
        const status = await this.validateFields();
        console.log("RESPOSNDW",status)
        if (status) {
            this._postUser();
        }
        // else {
        //     Alert.alert("Error", this.state.error);
        // }
        //  this.props.navigation.navigate('Shop_Login');
    }
    _uploadImage = async (image) => {

        console.log('Uploading Profile Image');
        const endPoint = 'image-upload';
        const body = {
            type: "user_profile",
            image: 'data:image/jpeg;base64,' + image.data
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'POST', body).then(response => {
            const { data } = response;
            console.log('DATA----------->', data);
            if (data.status) {
             //   this.setState({ image: data.data.url })
             //   console.log(this.state.image);
                console.log(data.message);
                this.setState({ profile_pic: data.data.image_name });
                ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
            } else {
                console.log(data.message);
            }
        })
            .catch((error) => {
                console.error(error);
            });
    }
    selectImage = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                this._uploadImage(response);
                this.setState({image:response})
            }
        });

    }
    onChange = (field, value) => {
        
        this.setState({ [field]: value });
    }
    setTime = (event, date) => {
        if (date === undefined) {
            // dismissedAction
        }
    };
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <KeyboardAvoidingView style={[globalStyles.subMainCont, { backgroundColor: colors.white }]}>
                    {/* <ImageBackground style={globalStyles.backgroundImage} source={require('../../../assets/images/login_bg.png')} resizeMode='stretch'> */}
                    <ScrollView style={{ flex: 7 }}>
                        <View style={{ flex: .7 }}>
                            <SignUpComponent
                                state={this.state}
                                selectImage={() => this.selectImage()}
                                onChange={(field, value) => this.onChange(field, value)} />
                            <ShopSignupComp
                                state={this.state}
                                onChange={(field, value) => this.onChange(field, value)}
                                showDatePicker={()=>this.showDatePicker()}
                                hideDatePicker={()=>this.hideDatePicker()}
                                handleConfirm={(date)=>this.handleConfirm(date)}
                                shopTimePicker={()=>this.shopTimePicker()}
                                handleShopClose={(date)=>this.handleShopClose(date)}
                                showDatePicker1={()=>this.showDatePicker1()}
                                hideDatePicker1={()=>this.hideDatePicker1()}/>
                        </View>
                        <View style={{ flex: .3 }}>
                            <SignUpButton
                                navigation={this.props.navigation}
                                handleSignUp={() => this.handleSignUp()} />
                        </View>
                    </ScrollView>
               </KeyboardAvoidingView>
            </View>
        )

    }
}
const mapStateToProps = state => ({
    user: state.UserReducer.user
});
export default connect(mapStateToProps)(ShopSignUp)