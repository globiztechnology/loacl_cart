import React, {Component} from 'react';
import {View, Text, FlatList, SafeAreaView} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {TouchableOpacity, TextInput} from 'react-native-gesture-handler';
import styles from './style';
import globalStyles from '../../../globalStyles';
import colors from '../../../globalStyles/colors';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';

class MyWallet extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: {
        backgroundColor: colors.primaryColor,
      },
      headerTitle: () => (
        <View style={globalStyles.viewRow}>
          <Text style={globalStyles.headerTitle}>My Wallet</Text>
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginLeft: hp('2%')}]}
          onPress={() => navigation.openDrawer('App2')}>
          <Icon3 name="menu" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, {marginRight: hp('2%')}]}>
          <Icon3 name="cart-outline" size={20} color="#fff" />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      Data: [
        {
          id: 1,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 2,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 3,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 4,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 5,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 6,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 7,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 8,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 9,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
        {
          id: 10,
          partyName: 'Fresh Eats & Groceries',
          date: '25 April 2020',
          time: '04:00 PM',
          debit: '+',
          credit: '-',
          amount: 100,
        },
      ],
    };
  }
  renderSeparator = () => {
    return <View style={styles.listSeparator} />;
  };

  render() {
    return (
      <SafeAreaView style={styles.responsiveBox}>
        <View style={styles.body}>
          <View style={styles.wltInfoSec}>
            <View style={styles.blncTtlWrap}>
              <Text style={styles.blncTtl}>Available Balance</Text>
            </View>
            <View style={styles.blncWrap}>
              <View style={styles.blncIcnWrap}>
                <Icon
                  name="rupee"
                  size={17}
                  color="#fff"
                  style={styles.blncIcn}
                />
              </View>
              <Text style={styles.blncAmt}>500</Text>
            </View>
            <View style={styles.btnCntnr}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('TermsScreen')}
                style={styles.adMnyBtn}>
                <Text style={styles.btnTxt}>Add money to wallet</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.wltAdMnySec}>
            <View style={styles.adMnyWrap}>
              <View style={styles.adMnyInputWrap}>
                <Icon
                  name="rupee"
                  size={20}
                  color="#515945"
                  style={styles.inptIcn}
                />
                <TextInput
                  style={styles.adMnyInput}
                  placeholder="Enter Amount"
                />
              </View>
              <View style={styles.payBtnWrap}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('PolicyScreen')}
                  style={styles.payBtn}>
                  <Text style={styles.btnTxt}>Pay</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.wltTrnsctnsSec}>
            <View style={styles.wltTrnsctnsWrap}>
              <View style={styles.wltTrnsctnsHedWrap}>
                <Text style={styles.wltTrnsctnsHed}>My Transactions</Text>
              </View>
              <View style={styles.wltTrnsctnsListWrap}>
                <FlatList
                  data={this.state.Data}
                  ItemSeparatorComponent={this.renderSeparator}
                  renderItem={({item}) => (
                    <View style={styles.trnsctnWrap}>
                      <View style={styles.trnsctnDtlWrap}>
                        <Text style={styles.trnsctnParty}>
                          {item.partyName}
                        </Text>
                        <View style={styles.trnsctnDtTm}>
                          <Text style={styles.trnsctnDt}>{item.date}</Text>
                          <Text style={styles.DtTmSprtr}>|</Text>
                          <Text style={styles.trnsctnTm}>{item.time}</Text>
                        </View>
                      </View>
                      <View style={styles.trnsctnAmtWrap}>
                        <Text style={styles.trnsctnInOut}>{item.debit}</Text>
                        <Icon
                          name="rupee"
                          color="#515945"
                          style={styles.trnsctnCrncy}
                        />
                        <Text style={styles.trnsctnAmt}>{item.amount}</Text>
                      </View>
                    </View>
                  )}
                  keyExtractor={item => item.id}
                />
              </View>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default MyWallet;
