import React, { Component } from 'react';
import globalStyles from '../../../globalStyles';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import OrderListComponent from '../../../reuseableComponents/orderList';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from '../../../globalStyles/colors';
import { TabView, SceneMap, TabViewAnimated, TabBar } from 'react-native-tab-view';
import { Image, View, Text, StatusBar, ToastAndroid } from 'react-native';
import NewOrders from './newOrder';
import Accepted from './accepted';
import Packed from './packed';
import Completed from './completed';
import Loader from '../../../reuseableComponents/loader';
import APICaller from '../../../utilities/apiCaller';
import { connect } from 'react-redux';


class ShopOrders extends Component {

    static navigationOptions = ({ navigation }) => {
        return {
            headerTitleAlign: 'center',
            headerStyle: {
                //    height:'100%',
                backgroundColor: colors.primaryColor,
            },
            headerTitle: () => (
                <View style={globalStyles.viewRow}>
                    <Text
                        style={globalStyles.headerTitle}>My Orders</Text>
                </View>
            ),
            headerLeft: () => (
                <TouchableOpacity
                    style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
                    onPress={() => navigation.openDrawer('App2')}>
                    {/* <Icon name="arrow-left" size={30} color='#fff' /> */}
                    <Image style={globalStyles.headerIcon} source={require('../../../assets/images/menu.png')} />
                </TouchableOpacity>

            ),
            headerRight: () => (
                <TouchableOpacity style={[globalStyles.viewRow, { marginRight: hp('2%') }]}>
                    <Image style={globalStyles.headerRightIcon} source={require('../../../assets/images/cart.png')} />
                </TouchableOpacity>
            ),
        };
    };
    detailOrder = (item) => {
        let id=item.id;
        this.props.navigation.navigate('PackOrder',{orderId:id});
    }
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'New Orders' },
            { key: 'second', title: 'Accepted' },
            { key: 'third', title: 'Packed' },
            { key: 'fourth', title: 'Completed' },
        ],
        accepted: [],
        pending: [],
        packed:[],
        completed:[],
        orders:[],
        show: false,
        item: '',
        status: 'pending',
        isLoader: false,
    };
    componentDidMount(){
        this.orderList('pending')
    }
    componentDidUpdate(prevProps, prevState) {
        if (prevState.index !== this.state.index) {
          if (this.state.index == 0) {
            this.orderList('pending')
          } else if(this.state.index==1){
            this.orderList('accepted')
          }
          else if(this.state.index==2){
              this.orderList('prepared')
          }
          else if(this.state.index==3){
              this.orderList('completed')
          }
        }
      }

      unselect_check = (item) => {
        for (let i = 0; i < this.state.orders.length; i++) {
            // console.log(comparePro[i].domain)
            if (this.state.orders[i].order_id == item.id) {
                this.state.orders.splice(i, 1);
                // img.splice(i, 1);
                let list = this.state.pending;
                item.checked = !item.checked;
                this.setState({ pending: list });
                // this.setState({ compareList: false });
                return (true);
            }
        }
    }
    checkbox_handle = (item) => {
       
        if (this.state.orders.length) {
            if (!this.unselect_check(item)) {
                // this.setState({ compareList: true });
                let list = this.state.pending;
                item.checked = !item.checked;
                this.setState({ pending: list });
                this.state.orders.push({order_id:item.id,status:'accepted'});
                // img.push(imgs);
            }
        } else {
            this.state.orders.push({order_id:item.id,status:'accepted'});
            // img.push(imgs);
            let list = this.state.pending;
            item.checked = !item.checked;
            this.setState({pending: list });
        }
        console.log("STATUS UPDATE ORDERS",this.state.orders)
    }
    uncheck_accepted = (item) => {
        for (let i = 0; i < this.state.orders.length; i++) {
            // console.log(comparePro[i].domain)
            if (this.state.orders[i].order_id == item.id) {
                this.state.orders.splice(i, 1);
                // img.splice(i, 1);
                let list = this.state.accepted;
                item.checked = !item.checked;
                this.setState({ accepted: list });
                // this.setState({ compareList: false });
                return (true);
            }
        }
    }
    check_accepted = (item) => {
       
        if (this.state.orders.length) {
            if (!this.uncheck_accepted(item)) {
                // this.setState({ compareList: true });
                let list = this.state.accepted;
                item.checked = !item.checked;
                this.setState({ accepted: list });
                this.state.orders.push({order_id:item.id,status:'accepted'});
                // img.push(imgs);
            }
        } else {
            this.state.orders.push({order_id:item.id,status:'accepted'});
            // img.push(imgs);
            let list = this.state.accepted;
            item.checked = !item.checked;
            this.setState({accepted: list });
        }
        console.log("STATUS UPDATE ORDERS",this.state.orders)
    }
    uncheck_packed = (item) => {
        for (let i = 0; i < this.state.orders.length; i++) {
            // console.log(comparePro[i].domain)
            if (this.state.orders[i].order_id == item.id) {
                this.state.orders.splice(i, 1);
                // img.splice(i, 1);
                let list = this.state.packed;
                item.checked = !item.checked;
                this.setState({ packed: list });
                // this.setState({ compareList: false });
                return (true);
            }
        }
    }
    check_packed = (item) => {
       
        if (this.state.orders.length) {
            if (!this.uncheck_accepted(item)) {
                // this.setState({ compareList: true });
                let list = this.state.packed;
                item.checked = !item.checked;
                this.setState({ packed: list });
                this.state.orders.push({order_id:item.id,status:'completed'});
                // img.push(imgs);
            }
        } else {
            this.state.orders.push({order_id:item.id,status:'completed'});
            // img.push(imgs);
            let list = this.state.packed;
            item.checked = !item.checked;
            this.setState({packed: list });
        }
        console.log("STATUS UPDATE ORDERS",this.state.orders)
    }
    _handleIndexChange = index => (
        this.setState({ index })
        );

    _renderHeader = props => <TabBar {...props} />;
    renderTabBar = props => (
        <TabBar
            {...props}
            // indicatorContainerStyle={{height:hp('.5%')}}
            indicatorStyle={{ backgroundColor: colors.secondColor, paddingVertical: hp('.4%'), borderTopLeftRadius: hp('1%'), borderTopRightRadius: hp('1%') }}
            activeColor={'#fff'}
            inactiveColor={'#fff'}
            labelStyle={{ fontSize: hp('1.5%'),textTransform:'capitalize'}}
            style={{ borderColor: colors.primaryColor, backgroundColor: colors.primaryColor,height:hp('6.5%') }}
        />
    );
    orderList = async (status) => {
        this.setState({isLoader:true})
        this.setState({orders:[]})
        console.log('ORDER LISTING');
        const options = {
          Authorization: 'Bearer ' + this.props.token,
          Accept: 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        };
        const endPoint = 'my-shop-orders?page=1&status='+status;
    
        const body = {
        };
        console.log('BODY=>>>', body);
        await APICaller(endPoint, 'GET', body, options)
          .then(async response => {
            const { data } = response;
            console.log("RESPONSE FROM SERVER===>", data);
            if (data.status) {
                if(status=='pending'){
                    this.setState({pending:data.data});
                }else if(status=='accepted'){
                    this.setState({accepted:data.data})
                }else if(status=='prepared'){
                    this.setState({packed:data.data});
                }else if(status=='completed'){
                    this.setState({completed:data.data})
                }
            }
            else {
                if(status=='pending'){
                    this.setState({pending:[]});
                }else if(status=='accepted'){
                    this.setState({accepted:[]})
                }else if(status=='prepared'){
                    this.setState({packed:[]});
                }else if(status=='completed'){
                    this.setState({completed:[]})
                }
            //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
            }
          })
          .catch(err => {
            console.log('Error from server', err);
          });
          this.setState({isLoader:false})
      }

    updateAcceptStatus = async () => {
        if(this.state.orders.length){
            this.setState({isLoader:true})
            console.log('ACCEPT STATUS');
            const options = {
              Authorization: 'Bearer ' + this.props.token,
              Accept: 'application/json, text/plain, */*',
              'Content-Type': 'application/json',
            };
            const endPoint = 'change-multiple-order-status';
        
            const body = {
                orders:this.state.orders
            };
            console.log('BODY=>>>', body);
            await APICaller(endPoint, 'POST', body, options)
              .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                  this.setState({orders:[]})
                  this.orderList('pending');
                  ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
                else {
                //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
              })
              .catch(err => {
                console.log('Error from server', err);
              });
              this.setState({isLoader:false})
        }
        else{
            ToastAndroid.showWithGravity("Select atleast one item", ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
      }
      updateCancelStatus = async (status) => {
        if(this.state.orders.length){
            this.setState({isLoader:true})
            for(let i=0;i<this.state.orders.length;i++){
                let array = this.state.orders;
                array[i] = { ...array[i], 'status':'rejected' };
                this.setState({ orders: array });
                //this.setState({orders[i],[status]:'rejected'})
            }
             console.log('REJECT STATUS',this.state.orders);
            const options = {
              Authorization: 'Bearer ' + this.props.token,
              Accept: 'application/json, text/plain, */*',
              'Content-Type': 'application/json',
            };
            const endPoint = 'change-multiple-order-status';
        
            const body = {
                orders:this.state.orders
            };
            console.log('BODY=>>>', body);
            await APICaller(endPoint, 'POST', body, options)
              .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                  this.setState({orders:[]})
                  this.orderList(status);
                  ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
                else {
                //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
              })
              .catch(err => {
                console.log('Error from server', err);
              });
              this.setState({isLoader:false})
        }
        else{
            ToastAndroid.showWithGravity("Select atleast one item", ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
      }
      updateCompleteStatus = async () => {
        if(this.state.orders.length){
            this.setState({isLoader:true})
            const options = {
              Authorization: 'Bearer ' + this.props.token,
              Accept: 'application/json, text/plain, */*',
              'Content-Type': 'application/json',
            };
            const endPoint = 'change-multiple-order-status';
        
            const body = {
                orders:this.state.orders
            };
            console.log('BODY=>>>', body);
            await APICaller(endPoint, 'POST', body, options)
              .then(async response => {
                const { data } = response;
                console.log("RESPONSE FROM SERVER===>", data);
                if (data.status) {
                  this.setState({orders:[]})
                  this.orderList('prepared');
                  ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
                else {
                //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
                }
              })
              .catch(err => {
                console.log('Error from server', err);
              });
              this.setState({isLoader:false})
        }
        else{
            ToastAndroid.showWithGravity("Select atleast one item", ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
      }
    render() {

        return (
            <View style={globalStyles.mainCont}>
                <StatusBar hidden={false} barStyle='dark-content' backgroundColor='#fff' />
                <View style={[globalStyles.subMainCont,{paddingVertical:hp('2%')}]}>
                    <TabView
                        renderTabBar={this.renderTabBar}
                        navigationState={this.state}
                        onIndexChange={this._handleIndexChange}
                        renderScene={SceneMap({
                            first: () => this.state.pending.length
                                ? <NewOrders state={this.state} 
                                checkbox_handle={(item) => this.checkbox_handle(item)} 
                                updateAcceptStatus={()=>this.updateAcceptStatus()}
                                updateCancelStatus={(status)=>this.updateCancelStatus(status)}/>
                                : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: hp('2%') }}>No New Order Avaliable</Text></View>,
                            second: () => this.state.accepted.length
                                ? <Accepted state={this.state}
                                    check_accepted={(item) => this.check_accepted(item)}
                                    detailOrder={(item) => this.detailOrder(item)}
                                    updateCancelStatus={(status)=>this.updateCancelStatus(status)}/>
                                : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: hp('2%') }}>No Accepted Order Avaliable</Text></View>,
                            third: () => this.state.packed.length
                                ? <Packed state={this.state}  
                                check_packed={(item) => this.check_packed(item)}
                                updateCancelStatus={(status)=>this.updateCancelStatus(status)}
                                updateCompleteStatus={()=>this.updateCompleteStatus()}/>
                                : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: hp('2%') }}>No Packed Order Avaliable</Text></View>,
                            fourth: () => this.state.completed.length
                                ? <Completed state={this.state}/>
                                : <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}><Text style={{ fontSize: hp('2%') }}>No Completed Order Avaliable</Text></View>,
                        })}

                    />


                </View>
                <Loader isLoader={this.state.isLoader}></Loader>
            </View>
            
        )
    }
}
const mapStateToProps = state => ({
    user: state.UserReducer.user,
    token: state.TokenReducer.token
});
export default connect(mapStateToProps)(ShopOrders);