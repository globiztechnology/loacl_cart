import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import CheckBox from 'react-native-check-box';
import colors from '../../../../globalStyles/colors';
import globalStyles from '../../../../globalStyles';
export const Accepted = props => {
    console.log(props.data)
    return (
        <View style={{ flex: 1 }}>
            <FlatList
                style={{ flex: 1, padding: hp('1.5%') }}
                data={props.state.accepted}
                renderItem={({ item }) => (
                    <TouchableOpacity style={{ height: hp('10.5%'), width: '100%', paddingVertical: hp('.8%') }}
                    onPress={()=>props.detailOrder(item)}>
                        <View style={{ flex: 1, backgroundColor: '#eaeaea', paddingLeft: hp('1.5%') }}>
                            <View style={globalStyles.mainCont}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: .1}}>
                                        <CheckBox
                                            style={{ flex: 1 ,justifyContent:'center'}}
                                            onClick={() => props.check_accepted(item)}
                                            isChecked={item.checked}
                                            checkBoxColor={colors.primaryColor}
                                        />
                                    </View>
                                    <View style={{ flex: .62, flexDirection: 'column'}}>
                                        <View style={{ flex: 1, }}>
                                            <View style={[globalStyles.subMainCont, { flexDirection: 'row' }]}>
                                                <View style={{ flex: .4, flexDirection: 'row', alignItems: 'center' }}>
                                                    <Text style={{
                                                        fontSize: hp('1.5%'),
                                                        textAlign: 'center',
                                                        color: '#999999',
                                                    }}>25 April 2020</Text>
                                                </View>
                                                <View style={{ flex: .1, flexDirection: 'column', alignItems: 'center', paddingVertical: hp('1%') }}>
                                                    <View style={{ width: hp('.1%'), height: '100%', backgroundColor: '#999999' }}></View>
                                                </View>
                                                <View style={{ flex: .5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: hp('1%') }}>
                                                    <Text style={{
                                                        fontSize: hp('1.5%'),
                                                        textAlign: 'center',
                                                        color: '#999999',
                                                    }}>04:00 PM</Text>
                                                </View>
                                            </View>

                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{
                                                fontSize: hp('2.3%'),
                                                fontWeight: 'bold',
                                                textAlign: 'center',
                                                color: '#3c3c3c',
                                            }}>Order Id: {item.id}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: .28, flexDirection: 'row' }}>
                                        <View style={[globalStyles.subMainCont,{}]}>
                                            <View style={ [styles.button]}>
                                                <Text style={[globalStyles.buttonText,{fontSize:hp('1.4%')}]}>Accepted</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </TouchableOpacity>
                )}
            />
            <View style={{position:'absolute',zIndex:100,bottom:hp('6%'),height:hp('6%'),left:0,right:0}}>
                <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity style={{width:hp('20%'),height:hp('5%'),marginLeft:hp('1%'),alignItems:'center',
                    justifyContent:'center',backgroundColor:colors.primaryColor,borderRadius:hp('2%'),opacity:.9}}
                    onPress={()=>props.updateCancelStatus('accepted')}>
                            <Text style={globalStyles.buttonText}>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>

    );
}

const styles = StyleSheet.create({
    button: {
        top: 0, right: 0, height: hp('4.2%'), backgroundColor:colors.secondColor, alignItems: 'center', justifyContent: 'center'
    }
})
export default Accepted;