import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import CheckBox from 'react-native-check-box';
import colors from '../../../../globalStyles/colors';
import globalStyles from '../../../../globalStyles';
export const Completed = props => {
    return (
        <View style={{ flex: 1 }}>
            <FlatList
                style={{ flex: 1, padding: hp('1.5%') }}
                data={props.state.completed}
                renderItem={({ item }) => (
                    <View style={{ height: hp('10.5%'), width: '100%', paddingVertical: hp('.8%') }}
                    >
                        <View style={{ flex: 1, backgroundColor: '#eaeaea', paddingLeft: hp('1.5%') }}>
                            <View style={globalStyles.mainCont}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{ flex: .72, flexDirection: 'column'}}>
                                        <View style={{ flex: 1, }}>
                                            <View style={[globalStyles.subMainCont, { flexDirection: 'row' }]}>
                                                <View style={{ flex: .4, flexDirection: 'row', alignItems: 'center' }}>
                                                    <Text style={{
                                                        fontSize: hp('1.5%'),
                                                        textAlign: 'center',
                                                        color: '#999999',
                                                    }}>25 April 2020</Text>
                                                </View>
                                                <View style={{ flex: .1, flexDirection: 'column', alignItems: 'center', paddingVertical: hp('1%') }}>
                                                    <View style={{ width: hp('.1%'), height: '100%', backgroundColor: '#999999' }}></View>
                                                </View>
                                                <View style={{ flex: .5, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start', paddingLeft: hp('1%') }}>
                                                    <Text style={{
                                                        fontSize: hp('1.5%'),
                                                        textAlign: 'center',
                                                        color: '#999999',
                                                    }}>04:00 PM</Text>
                                                </View>
                                            </View>

                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{
                                                fontSize: hp('2.3%'),
                                                fontWeight: 'bold',
                                                textAlign: 'center',
                                                color: '#3c3c3c',
                                            }}>Order Id: {item.id}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: .28, flexDirection: 'row' }}>
                                        <View style={[globalStyles.subMainCont,{}]}>
                                            <View style={ [styles.button]}>
                                                <Text style={[globalStyles.buttonText,{fontSize:hp('1.4%')}]}>completed</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>

    );
}

const styles = StyleSheet.create({
    button: {
        top: 0, right: 0, height: hp('4.2%'), backgroundColor:colors.secondColor, alignItems: 'center', justifyContent: 'center'
    }
})
export default Completed;