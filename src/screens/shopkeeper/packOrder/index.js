import React, { Component, useState } from 'react';
import globalStyles from '../../../globalStyles';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  Modal,
  Button,
  ToastAndroid,
  KeyboardAvoidingView,
  Linking,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native-gesture-handler';
import CheckBox from 'react-native-check-box';
import colors from '../../../globalStyles/colors';
import styles from './style';
import Icon2 from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/SimpleLineIcons';

import { createBottomTabNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import Loader from '../../../reuseableComponents/loader';
import APICaller from '../../../utilities/apiCaller';

const ACTIVE_TAB_COLOR = '#60C3FF';
const INACTIVE_TAB_COLOR = '#aaa';



class PackOrder extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        //    height:'100%',
        backgroundColor: colors.primaryColor,
      },
      headerTitle: () => (
        <View style={globalStyles.viewRow}>
          <Text style={globalStyles.headerTitle}>Order Detail</Text>
        </View>
      ),
      headerLeft: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, { marginLeft: hp('2%') }]}
          onPress={() => navigation.goBack()}>
          <Icon3 name="arrow-left" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          style={[globalStyles.viewRow, { marginRight: hp('2%') }]}>
          <Icon3 name="cart-outline" size={20} color="#fff" />
        </TouchableOpacity>
      ),
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      order_id: props.navigation.getParam('orderId', 'id'),
      data: [],
      order: '',
      customer: '',
      amount:'',
      items:[],
      notes:''
    };
  }
  componentDidMount() {
    this.orderDetail()
  }

  unselect_check = (item) => {
    for (let i = 0; i < this.state.items.length; i++) {
        // console.log(comparePro[i].domain)
        if (this.state.items[i].id == item.id) {
            this.state.items.splice(i, 1);
            // img.splice(i, 1);
            let list = this.state.order;
            item.checked = !item.checked;
            this.setState({ order: list });
            // this.setState({ compareList: false });
            return (true);
        }
    }
}
checkbox_handle = (item) => {
   
    if (this.state.items.length) {
        if (!this.unselect_check(item)) {
            // this.setState({ compareList: true });
            let list = this.state.order;
            item.checked = !item.checked;
            this.setState({ order: list });
            this.state.items.push(item);
            // img.push(imgs);
        }
    } else {
        this.state.items.push(item);
        // img.push(imgs);
        let list = this.state.order;
        item.checked = !item.checked;
        this.setState({order: list });
    }
    console.log("STATUS UPDATE ORDERS",this.state.items)
}
  orderDetail = async () => {
    this.setState({ isLoader: true })
    console.log('ACCEPT STATUS');
    const options = {
      Authorization: 'Bearer ' + this.props.token,
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    };
    const endPoint = 'order-details';

    const body = {
      order_id: this.state.order_id,
    };
    console.log('BODY=>>>', body);
    await APICaller(endPoint, 'POST', body, options)
      .then(async response => {
        const { data } = response;
        console.log("RESPONSE FROM SERVER===>", data);
        if (data.status) {
          this.setState({ data: data.data.order_item });
          this.setState({ order: data.data });
          this.setState({ customer: data.data.customer_details })
        }
        else {
          //   ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
      })
      .catch(err => {
        console.log('Error from server', err);
      });
    this.setState({ isLoader: false })
  }
  makeCall = () => {
    if (this.state.customer != '') {
      console.log("Contact Number", this.state.customer.mobile_no)
      const phoneNumber = this.state.customer.mobile_no;
      Linking.canOpenURL(`tel:${phoneNumber}`)
        .then(supported => {
          if (!supported) {
            ToastAndroid.showWithGravity('Not a valid phone number', ToastAndroid.SHORT, ToastAndroid.CENTER);
          } else {
            return Linking.openURL(`tel:${phoneNumber}`);
          }
        })
    }
    else {
      ToastAndroid.showWithGravity('Please select a shop', ToastAndroid.SHORT, ToastAndroid.CENTER);
    }
  }
  renderHeader = () => {
    return (
      <View style={styles.srchShopHedWrap}>
        <View
          style={{ flex: 1, flexDirection: 'row', paddingHorizontal: wp('4') }}>
          <View
            style={{
              flex: 0.8,
              alignItems: 'flex-start',
              justifyContent: 'center',
              paddingVertical: hp('2'),
            }}>
            <View style={{ flex: 1 }}>
              <View style={{ flex: 0.8 }}>
                <View style={styles.cstmrNameWrap}>
                  <Text style={styles.srchShopTxt}>{this.state.customer.name}</Text>
                </View>
              </View>
              <View style={{ flex: 0.2 }}>
                <View style={styles.ordrTimeWrap}>
                  <Icon2 name="clock" size={15} color="#e97657" />
                  <Text style={styles.ordrTime}>{this.state.order.updated_at}</Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 0.2,
              alignItems: 'flex-end',
              justifyContent: 'center',
              paddingVertical: hp('2'),
            }}>
            <TouchableOpacity
              onPress={() => this.makeCall()}
              style={styles.phnIcnWrap}>
              <Icon2
                name="phone"
                size={24}
                color="#fff"
                style={styles.phoneIcn}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };


  renderFooter = () => {
    return (
      <View style={{ marginBottom: hp('11') }}>
        <KeyboardAvoidingView style={{ flex: 1, alignItems: 'center' }}>
          <View>
            {this.state.order.notes
              ? <View style={styles.cstmrCmntWrap}>
                <Text style={styles.cstmrCmntHed}>Customer Comments: </Text>
                <Text style={styles.cstmrCmnt}>
                  {this.state.order.notes}
                </Text>
              </View>
              : null}
          </View>
          <View>
            <View style={styles.notesWrap}>
              <Text style={styles.notesLbl}>Enter Your Notes:</Text>
              <TextInput style={styles.noteText}
               value={this.state.notes}
               onChangeText={(notes)=>this.setState({notes:notes})} />
            </View>
          </View>
          <View>
            <View style={styles.amntWrap}>
              <TextInput
                placeholderTextColor="#515943"
                keyboardType={"numeric"}
                placeholder="Enter Amount"
                style={styles.amntText}
                value={this.state.amount}
                onChangeText={(amount)=>this.setState({amount:amount})}
              />
            </View>
          </View>
          <View>
            <View style={styles.btnContainer}>
              <View style={styles.btnWrap}>
                <TouchableOpacity
                  onPress={() =>this.updatePackedStatus() }
                  style={styles.btn}>
                  <Text style={styles.btnTxt}>Pack Order</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  };
  toggleModal = () => {
    this.setState({ visible: !this.state.visible });
  };
  updatePackedStatus = async () => {
    if(this.state.items.length!=this.state.data.length){
      ToastAndroid.showWithGravity("All items are not packed", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return;
    }else if(this.state.amount==''){
      ToastAndroid.showWithGravity("Please fill the amount", ToastAndroid.SHORT, ToastAndroid.CENTER);
      return;
    }else{
    this.setState({isLoader:true})
    console.log('ACCEPT STATUS');
    const options = {
      Authorization: 'Bearer ' + this.props.token,
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    };
    const endPoint = 'change-order-status';

    const body = {
        order_id:this.state.order.id,
        status:'prepared',
        amount:this.state.amount,
        store_owner_note:this.state.notes
    };
    console.log('BODY=>>>', body);
    await APICaller(endPoint, 'POST', body, options)
      .then(async response => {
        const { data } = response;
        console.log("RESPONSE FROM SERVER===>", data);
        if (data.status) {
           ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER); 
           this.props.navigation.goBack(); 
        }
        else {
           ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.CENTER);
        }
      })
      .catch(err => {
        console.log('Error from server', err);
      });
      this.setState({isLoader:false})
    }
  }
  item=({ item,index})=>{
    return (
      <View style={styles.crd}>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 0.3 }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                marginBottom: hp('1%'),
                borderBottomWidth: 1,
                borderBottomColor: '#e1e1e1',
              }}>
              <View style={{ flex: 0.8 }}>
                <View style={styles.prdctNameWrap}>
                  <CheckBox
                    style={styles.checkbox}
                    onClick={() => this.checkbox_handle(item)}
                    isChecked={item.checked}
                    checkBoxColor={colors.primaryColor}
  
                  />
                  <Text style={styles.crdTtl}>{item.item_name}</Text>
                </View>
              </View>
              <View style={{ flex: 0.2, justifyContent: 'center' }}>
                {item.weight
                  ?
                  <View style={styles.qtyWrap}>
                    <Text style={styles.qty}>wgt: {item.weight}</Text>
                  </View>
                  : null}
                {item.quantity
                  ?
                  <View style={styles.qtyWrap}>
                    <Text style={styles.qty}>qty: {item.quantity}</Text>
                  </View>
                  : null}
              </View>
            </View>
          </View>
          <View style={{ flex: 0.7 }}>
            <View style={{ flex: 1 }}>
              <View style={styles.descWrap}>
                <Text style={styles.note}>Note: {'\n'} </Text>
                {item.comment
                  ? <Text style={styles.desc}>{item.comment}</Text>
                  : <Text style={styles.desc}>Note is not available</Text>}
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
  
  render() {
    console.log(this.state.order)
    return (
      <SafeAreaView>
        <View style={styles.responsiveBox}>
          <KeyboardAvoidingView style={styles.mainContainer}>
         
            <FlatList
              data={this.state.data}
              ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
              renderItem={this.item}
              keyExtractor={item => item.id}
            />

          </KeyboardAvoidingView>
        </View>
        <Loader isLoader={this.state.isLoader}></Loader>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => ({
  user: state.UserReducer.user,
  token: state.TokenReducer.token
});

export default connect(mapStateToProps)(PackOrder);
