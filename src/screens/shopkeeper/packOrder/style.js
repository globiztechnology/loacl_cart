import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  responsiveBox: {
    width: wp('100%'),
    height: hp('100%'),
    backgroundColor: '#fff',
  },
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  // FlatList Header Style
  srchShopHedWrap: {
    flex: 1,
    paddingVertical: hp('1%'),
  },
  srchShopTxt: {
    color: '#515a45',
    fontSize: hp('2.7%'),
  },
  ordrTimeWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp('0.7%'),
  },
  ordrTime: {
    color: '#525945',
    fontSize: hp('1.2%'),
    marginHorizontal: wp('1%'),
  },
  phnIcnWrap: {
    width: hp('5%'),
    height: hp('5%'),
    borderRadius: hp('2.5%'),
    backgroundColor: '#bdb85a',
  },
  phoneIcn: {
    textAlign: 'center',
    paddingVertical: hp('1%'),
  },
  // FlatList Content Style
  crd: {
    elevation: 10,
    borderWidth: 1,
    borderRadius: 5,
    shadowRadius: 5,
    shadowOpacity: 0.5,
    shadowColor: '#000',
    marginBottom: hp('2%'),
    borderColor: '#e1e1e1',
    backgroundColor: '#fff',
    marginHorizontal: wp('3%'),
    paddingVertical: hp('0.1%'),
    paddingHorizontal: wp('4%'),
    shadowOffset: {width: 0, height: 3},
  },
  prdctNameWrap: {
    height: hp('5%'),
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  checkbox: {
    alignSelf: 'center',
    marginLeft: wp('-1.6%'),
  },
  crdTtl: {
    alignSelf: 'center',
    fontSize: hp('2%'),
    margin: 1,
  },
  qtyWrap: {
    height: hp('2%'),
  },
  qty: {
    fontSize: hp('1.7%'),
    textAlign: 'right',
  },
  note: {
    height: hp('3.2%'),
    fontSize: hp('2%'),
    color: '#515943',
  },
  desc: {
    color: '#919489',
    fontSize: hp('1.5%'),
    lineHeight: hp('2.3%'),
    marginBottom: hp('1%'),
  },
  //   Flatlist Style Ends
  cstmrCmntWrap: {
    width: wp('94%'),
    backgroundColor: '#eee',
    marginTop: hp('1%'),
    paddingHorizontal: wp('4%'),
    paddingVertical: hp('1%'),
  },
  cstmrCmntHed: {
    marginBottom: hp('0.4%'),
    fontSize: hp('2%'),
    color: '#515943',
  },
  cstmrCmnt: {
    color: '#919489',
    fontSize: hp('1.5%'),
    lineHeight: hp('2.3%'),
    fontFamily: 'PoppinsBold',
    marginBottom: hp('1%'),
  },
  notesWrap: {
    width: wp('94%'),
    height: hp('10%'),
    paddingTop: hp('1%'),
    paddingLeft: wp('4%'),
    marginTop: hp('2.7%'),
    marginBottom: hp('1%'),
    backgroundColor: '#eee',
  },
  notesLbl: {
    color: '#515943',
    fontSize: hp('2%'),
  },
  amntWrap: {
    width: wp('94%'),
    paddingLeft: wp('4%'),
    backgroundColor: '#eee',
  },
  amntText: {
    fontSize: hp('2%'),
  },
  btnContainer: {
    paddingTop: hp('3%'),
  },
  btn: {
    backgroundColor: '#e97657',
    borderRadius: 50,
    width: wp('50%'),
  },
  btnTxt: {
    textTransform: 'uppercase',
    color: '#fff',
    textAlign: 'center',
    fontSize: hp('2%'),
    paddingVertical: hp('1.7%'),
  },
  // Modal Style
  mdlBackground: {
    flex: 1,
    backgroundColor: '#000000aa',
  },
  mdlWrap: {
    backgroundColor: '#fdfbef',
    marginTop: hp('43%'),
    paddingVertical: hp('1%'),
    paddingHorizontal: wp('3%'),
  },
  toggleIcn: {
    alignItems: 'center',
    position: 'absolute',
    top: -35,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 9999,
  },
  mdlArrwWrap: {
    width: hp('6'),
    height: hp('6'),
    borderRadius: hp('3'),
    backgroundColor: '#e97657',
  },
  mdlArrw: {
    textAlign: 'center',
    paddingVertical: hp('1'),
  },
  mdlCrd: {
    paddingHorizontal: wp('4'),
    paddingVertical: hp('2'),
    marginBottom: hp('2'),
    marginHorizontal: wp('3'),
    borderRadius: 3,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 1,
  },
});

export default styles;
