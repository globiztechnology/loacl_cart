import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  resposiveBox: {
    width: wp('100%'),
    height: hp('100%'),
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentSec: {
    paddingHorizontal: wp('4'),
    paddingVertical: hp('2'),
  },
  heading: {
    color: '#505843',
    fontWeight: 'bold',
    fontSize: hp('2.5'),
    marginVertical: hp('1.3'),
  },
  description: {
    color: '#525a45',
    fontSize: hp('2'),
    textAlign: 'justify',
    lineHeight: hp('3'),
  },
});

export default styles;