import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {View, Text, Image, ImageBackground} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import globalStyles from '../../../globalStyles';
import styles from './style';

class PackSuccess extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={globalStyles.mainCont}>
        <View style={globalStyles.subMainCont}>
          <ImageBackground
            style={globalStyles.backgroundImage}
            source={require('../../../assets/images/bg.png')}
            resizeMode="stretch">
            <View style={globalStyles.mainCont}>
              <View style={{flex: 0.16}} />
              <View
                style={{
                  flex: 0.5,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={styles.successImgWrap}>
                  <Image
                    style={styles.successImg}
                    source={require('../../../assets/images/order_success.png')}
                  />
                </View>
                <View style={styles.successTitleWrap}>
                  <Text style={styles.successTitle}>Successful !</Text>
                </View>
                <View style={styles.successDescWrap}>
                  <Text style={styles.textStyle}>
                    Order <Text style={{fontWeight: 'bold'}}>#14573</Text> has
                    been packed successfully.
                  </Text>
                </View>
                <View style={styles.successBtnWrap}>
                  <TouchableOpacity
                    style={{
                      paddingHorizontal: hp('3.6%'),
                      paddingVertical: hp('1.3%'),
                      borderRadius: hp('2.5%'),
                      backgroundColor: '#bcb85a',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}
                    onPress={() =>
                      this.props.navigation.navigate('Notification')
                    }>
                    <Text
                      style={[
                        globalStyles.buttonText,
                        {
                          letterSpacing: wp('0.5%'),
                        },
                      ]}>
                      Back to orders
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{flex: 0.46}} />
            </View>
          </ImageBackground>
        </View>
      </View>
    );
  }
}

export default PackSuccess;
