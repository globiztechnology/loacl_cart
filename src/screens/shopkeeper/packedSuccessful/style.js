import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  successImgWrap: {
    marginBottom: hp('2'),
  },
  successImg: {
    width: hp('12'),
    height: hp('12'),
    resizeMode: 'cover',
  },
  successTitleWrap: {
    paddingVertical: hp('1.5'),
  },
  successTitle: {
    fontSize: hp('3'),
  },
  successDescWrap: {
    paddingTop: hp('1.5'),
    paddingVertical: hp('3'),
    width: wp('75'),
  },
  textStyle: {
    fontSize: hp('2'),
    color: '#525a45',
    lineHeight: hp('2'),
    textAlign: 'center',
  },
  successBtnWrap: {
    marginTop: hp('2'),
  },
});

export default styles;
