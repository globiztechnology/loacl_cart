import React, { Component } from 'react';
import { Image, ImageBackground, StyleSheet, View, BackHandler, ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import { setUserData, setToken } from '../../redux/actions/user';
import AsyncStorage from '@react-native-community/async-storage';
import {colors} from '../../globalStyles/colors';
let backPressed = 0;
export class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
    }
  }
  disableBackButton = () => {
    if(backPressed > 0){
        console.log(backPressed);
        BackHandler.exitApp();
        backPressed = 0;
    }else {
        console.log(backPressed);
        backPressed++;
        ToastAndroid.show("Press again to exit the app", ToastAndroid.SHORT);
        setTimeout( () => { backPressed = 0}, 2500);
        return true;
    }
 
  }
  
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.disableBackButton);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.disableBackButton);
  }
  Hide_Splash_Screen = () => {
    this.setState({ isVisible: false });
   // this.props.navigation.navigate('Slider');
  }

  componentDidMount() {
    var that = this;
    setTimeout(() =>{
     // this.props.navigation.navigate('Login');
      this._initialLoadFunction();
    }, 3700);
  }
  _initialLoadFunction = async () => {
    await AsyncStorage.getItem('token').then(response => {
      if (JSON.parse(response)) {
        console.log('TOKEN AV')
        this.props.dispatch(setToken(JSON.parse(response)));
         AsyncStorage.getItem('userData').then(response => {
          if (JSON.parse(response)) {
            console.log('If block');
            this.Hide_Splash_Screen();
            console.log('Splash', JSON.parse(response));
            this.props.dispatch(setUserData(JSON.parse(response)));
            if (this.props.user.user_type == 'shopkeeper') {
              this.props.navigation.navigate('ShopOrder');
            } else {
              this.props.navigation.navigate('Welcome');
            }
          }
          else { console.log('USER EMPTY') }
        });
      } else {
        console.log('TOKEN EMPTY')
        this.props.navigation.navigate('Login');
      }
    });
  };

  render() {
    let Splash_Screen = (
      <View style={styles.SplashScreen_RootView}>
        <ImageBackground source={require('../../assets/images/bg.png')}
          style={{ width: '100%', height: '100%',backgroundColor: colors.white}} resizeMode='stretch'>
          <View style={{flex:.2}}></View>
          <View style={{flex:.4,alignItems:'center',justifyContent:'flex-start'}}>
            <Image source={require('../../assets/images/logo.png')} 
            style={{position:'absolute', width: '100%', height: '80%',alignSelf:'center'}}
            resizeMode='stretch'
            />
          </View>
          <View style={{flex:.4}}></View>
        </ImageBackground>
      </View>)
    return (
      <View style={styles.MainContainer}>
        {
          (this.state.isVisible === true) ? Splash_Screen : null
        }
      </View>);
  }
}

const mapStateToProps = state => ({
  user: state.UserReducer.user
});
export default connect(mapStateToProps)(SplashScreen);

const styles = StyleSheet.create({
  MainContainer: {
    width: '100%',
    height: '100%',
  },
  SplashScreen_RootView: {
    justifyContent: 'center',
    flex: 1
  },
  splashImage: {
    width: '100%',
    height: '100%'
  },
});