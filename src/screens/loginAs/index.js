import React, { Component } from 'react';
import { View, ImageBackground, Image, Text } from 'react-native';
import globalStyles from '../../globalStyles';
import loginAsStyle from './loginAsStyle';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
class LoginAs extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <View style={globalStyles.mainCont}>
                <View style={globalStyles.subMainCont}>
                    <ImageBackground style={globalStyles.backgroundImage} source={require('../../assets/images/bg.png')} resizeMode='stretch'>
                        <View style={globalStyles.mainCont}>
                            <View style={{ flex: .16}}></View>
                            <View style={{ flex: .38 }}>
                                <View style={loginAsStyle.subMainCon}>
                                    <View style={loginAsStyle.logoMainCon}>
                                        <View style={loginAsStyle.logoSubCon}>
                                        <View style={loginAsStyle.logoStyle}>
                                            <Image style={globalStyles.imageStyle} resizeMode='stretch' source={require('../../assets/images/logo.png')} />
                                        </View>
                                        </View>
                                    </View>
                                    <View style={loginAsStyle.textCon}>
                                        <View style={globalStyles.view}>
                                            <Text style={loginAsStyle.textStyle}>I want to register as..</Text>
                                        </View>
                                    </View>
                                    <View style={loginAsStyle.buttonsCon}>
                                        <View style={globalStyles.view}>
                                            <View style={loginAsStyle.buttonSub}>
                                                <TouchableOpacity style={[globalStyles.buttonStyle2, {paddingHorizontal: hp('6%')}]} onPress={()=> this.props.navigation.navigate('Cust_Signup')}> 
                                                    <Text style={globalStyles.buttonText}>customer</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={loginAsStyle.buttonSub}>
                                                <TouchableOpacity style={globalStyles.buttonStyle1} onPress={()=> this.props.navigation.navigate('Shop_Signup')}>
                                                    <Text style={globalStyles.buttonText}>shopkeeper</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: .46, }}></View>

                        </View>
                    </ImageBackground>
                </View>
            </View>

        );
    }
}

export default LoginAs;