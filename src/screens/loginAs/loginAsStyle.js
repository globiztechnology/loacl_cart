import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
    subMainCon: {
        flex: 1,
        alignItems: 'center'
    },
    logoMainCon: {
        flex: .48,
        flexDirection: 'row'
    },
    logoSubCon: {
        flex: 1,
        flexDirection: 'column-reverse'
    },
    logoStyle: {
        height: '100%',
        width: '60%',
        alignSelf: 'center'
    },
    textCon: {
        flex: .12
    },
    buttonsCon: {
        flex: .4,
        paddingVertical: hp('1%')
    },
    buttonSub: {
        flex: .5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: hp('3.1%'),
        textAlign: 'center',
        lineHeight: hp('5%'),
        color: '#665d5d',
        fontWeight: 'normal'
    }
});