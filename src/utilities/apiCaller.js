import axios from 'axios';
const hostName = 'https://globizcloudserver.com/localcart/api/';
const APICaller = (endpoint, method, body,options) =>
  axios({
    url: hostName + endpoint,
    method: method || 'GET',
    data: body,
    headers:options,
    responseType: 'json',
  })
    .then(response => {
      console.log(`response from ${endpoint} >> ${response}`);
      return response;
    })
    .catch(error => {
      console.log(`Error from ${endpoint} >> ${error}`);
      throw error.response;
    });

export default APICaller;
