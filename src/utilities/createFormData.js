const createFormData = (photo, body) => {
    const data = new FormData();
  
    data.append("image", {
      name: photo.fileName,
      type: photo.type,
      uri: photo.path,
    });
  
    Object.keys(body).forEach(key => {
      data.append(key, body[key]);
    });
  
    return data;
  };
  export default createFormData;