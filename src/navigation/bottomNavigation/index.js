import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DrawerScreen from '../../screens/drawer';
import CreateOrderScreen from '../../screens/customer/createOrder';

const AppNavigation = createStackNavigator(
    {
      CreateOrder: {screen: CreateOrderScreen},
    
    },
    {
        initialRouteName: 'CreateOrder',
    },
    );
const BottomNavigation = createBottomTabNavigator(
    {
      drawerHome: {screen: AppNavigation},
    },
    {
      initialRouteName: 'drawerHome',
      contentComponent: DrawerScreen,
    //   drawerType: 'front',
    //   drawerPosition: 'left',
    //   drawerWidth: wp('70%'),
    //   drawerBackgroundColor:'transparent',
    //   edgeWidth: -200,
      style: { 
        // borderBottomLeftRadius:hp('2%'),
        // borderTopRightRadius:hp('8%'),
        // borderBottomRightRadius:hp('8%'),
        // borderColor:'#A8D166',
        // borderRightWidth: hp('1.2%'),
        // borderBottomWidth: hp('1.2%'),
        // borderTopWidth: hp('1.2%'),
        backgroundColor:'#ffffff'
    }
      // minSwipeDistance:10
    },
    );
    export default BottomNavigation;