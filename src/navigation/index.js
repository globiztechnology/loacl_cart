import React, { useEffect } from 'react';
import { createSwitchNavigator, createAppContainer} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from '../screens/splashScreen';
import LoginAs from '../screens/loginAs/index';
import WelcomeScreen from '../screens/customer/welcome';
import CustomerSignUp from '../screens/customer/signup';
import ShopSignUp from '../screens/shopkeeper/signup';
import OrderPlaced from '../screens/customer/orderPlaced';
import DrawerNavigation from './drawerNavigation';
import BottomNavigation from './bottomNavigation';
import LoginScreen from '../screens/loginScreen';
import { BackHandler } from 'react-native';
import ShopDrawerNavigation from './drawerNavigation/shop';

const stackNavigator = createStackNavigator(
    {
        Splash: { screen: SplashScreen, navigationOptions: { headerShown: false } },
        Login: { screen: LoginScreen, navigationOptions: { headerTitleAlign: 'center' } },
        LoginAs: { screen: LoginAs, navigationOptions: { headerShown: false } },
  

        //    Customer
        Cust_Signup: { screen: CustomerSignUp, navigationOptions: { headerTitleAlign: 'center' } },
        Welcome: { screen: WelcomeScreen, navigationOptions: { headerShown: false } },
      

        //  Shopkeeper
        Shop_Signup: { screen: ShopSignUp, navigationOptions: { headerTitleAlign: 'center' } },
    },
    {
        initialRouteName: 'Splash',
    },

);

const switchNavigator = createSwitchNavigator(
    {
        AppStartup: { screen: stackNavigator },
        App: { screen: DrawerNavigation },
        App2:{screen: ShopDrawerNavigation}
    }
)

const appContainer = createAppContainer(switchNavigator);

// const defaultStackGetStateForAction =
//   stackNavigator.router.getStateForAction;

// stackNavigator.router.getStateForAction = (action, state) => {
//     console.log(state);
//   if(state.index === 0 && action.type === NavigationActions.BACK){
//     BackHandler.exitApp();
//     return null;
//   }

//   return defaultStackGetStateForAction(action, state);
// };

export default appContainer;