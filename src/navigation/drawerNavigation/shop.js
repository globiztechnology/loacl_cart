import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ShopDrawerScreen from '../../screens/drawer/shop';
import ShopOrders from '../../screens/shopkeeper/orderList';
import FAQ from '../../screens/shopkeeper/faq';
import Terms from '../../screens/shopkeeper/terms';
import About from '../../screens/shopkeeper/about';
import Policy from '../../screens/shopkeeper/policy';
import MyWallet from '../../screens/shopkeeper/myWallet';
import PackOrder from '../../screens/shopkeeper/packOrder';
import PackSuccess from '../../screens/shopkeeper/packedSuccessful';
import ShopkeeperProfile from '../../screens/shopkeeper/profile/index'

const AppNavigation = createStackNavigator(
    {
      ShopOrder:  {screen: ShopOrders},
      Profile:{screen:ShopkeeperProfile},
      Faq:{screen:FAQ},
      Terms:{screen:Terms},
      About:{screen:About},
      Privacy:{screen:Policy},
      Wallet:{screen:MyWallet},
      PackOrder:{screen:PackOrder},
      Packed:{screen:PackSuccess},
    },
    {
        initialRouteName: 'ShopOrder',
    },
    );
const ShopDrawerNavigation = createDrawerNavigator(
    {
      drawerHome: {screen: AppNavigation},
      
    },
    {
      initialRouteName: 'drawerHome',
      contentComponent: ShopDrawerScreen,
      drawerType: 'front',
      drawerPosition: 'left',
      drawerWidth: wp('75%'),
      drawerBackgroundColor:'transparent',
      edgeWidth: -200,
      style: { 
        backgroundColor:'#ffffff'
    }
      // minSwipeDistance:10
    },
    );
    export default ShopDrawerNavigation;