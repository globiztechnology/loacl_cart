import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DrawerScreen from '../../screens/drawer';
import CreateOrderScreen from '../../screens/customer/createOrder';
import MyOrders from '../../screens/customer/myOrders';
import OrderPlaced from '../../screens/customer/orderPlaced';
import CustomerFAQ from '../../screens/customer/faq';
import CustomerWallet from '../../screens/customer/myWallet';
import CustomerPolicy from '../../screens/customer/policy';
import CustomerTerms from '../../screens/customer/terms';
import CustomerProfile from '../../screens/customer/profile/index';
import CustomerAbout from '../../screens/customer/about';

const AppNavigation = createStackNavigator(
    {
      CreateOrder: {screen: CreateOrderScreen},
      MyOrder:  {screen: MyOrders},
      Success: { screen: OrderPlaced, navigationOptions: { headerShown: false } },
      CustFaq:  {screen: CustomerFAQ},
      CustWallet: {screen: CustomerWallet},
      CustAbout:{screen: CustomerAbout},
      CustPrivacy:{screen:CustomerPolicy},
      CustTerm:{screen:CustomerTerms},
      CustProfile:{screen:CustomerProfile},
    },
    {
        initialRouteName: 'CreateOrder',
    },
    );
const DrawerNavigation = createDrawerNavigator(
    {
      drawerHome: {screen: AppNavigation},
      
    },
    {
      initialRouteName: 'drawerHome',
      contentComponent: DrawerScreen,
      drawerType: 'front',
      drawerPosition: 'left',
      drawerWidth: wp('75%'),
      drawerBackgroundColor:'transparent',
      edgeWidth: -200,
      style: { 
        backgroundColor:'#ffffff'
    }
      // minSwipeDistance:10
    },
    );
    export default DrawerNavigation;