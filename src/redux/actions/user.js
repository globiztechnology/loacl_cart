import ActionType from '../constants/index';

export const setUserData = param => {
  return {
    type: ActionType.SET_USER_DATA,
    payload: param,
  };
};

export const setToken = param => {
  return {
    type: ActionType.SET_TOKEN,
    payload: param,
  };
};
