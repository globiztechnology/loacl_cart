import ActionType from '../constants/index';
const initialState = {
  token: {},
};

const TokenReducer = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.SET_TOKEN:
     
      return Object.assign({}, state, {
        token: action.payload,
      });
    default:
      return state;
  }
}
export default TokenReducer;
