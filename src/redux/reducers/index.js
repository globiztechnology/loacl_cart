import {combineReducers} from 'redux';
import UserReducer from './user';
import TokenReducer from './token';
const appReducer = combineReducers({
  UserReducer,
  TokenReducer,
});
const rootReducer = (state, action) => {
  return appReducer(state, action);
};
export default rootReducer;
