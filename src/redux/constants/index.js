import keyMirror from 'fbjs/lib/keyMirror';

const ActionTypes = keyMirror({
  STORE_SERVICE_LIST_DATA: 'STORE_SERVICE_LIST_DATA',
  SET_CHECKIN_DATE: 'SET_CHECKIN_DATE',
  SET_CHECKOUT_DATE: 'SET_CHECKOUT_DATE',
  SET_UNIT: 'SET_UNIT',
  SET_USER_DATA: 'SET_USER_DATA',
  SET_TOKEN:'SET_TOKEN',
});

export default ActionTypes;
