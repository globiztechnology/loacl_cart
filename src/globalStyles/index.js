import { StyleSheet } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import colors from './colors';

export default StyleSheet.create({
    mainCont: {
        width: '100%',
        height: '100%',
    },
    subMainCont: {
        flex: 1,
    },
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        flex: 1,
        flexDirection: 'row'
    },
    viewRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerTitle: {
        color: colors.white,
        fontSize: hp('2.1%'),
        textAlign: 'center',
        // fontWeight:'800',
        textTransform:'uppercase'
    },
    headerIcon:{height:hp('2%'),width:hp('2%')},
    headerRightIcon:{height:hp('2.5%'),width:hp('2.5%')},
    backgroundImage: {
        width: '100%',
        height: '100%',
        backgroundColor: colors.white,
    },
    buttonStyle1: {
        paddingHorizontal: hp('5%'),
        paddingVertical: hp('1.1%'),
        borderRadius: hp('2.1%'),
        backgroundColor: colors.primaryColor,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonStyle2: {
        paddingVertical: hp('1.1%'),
        borderRadius: hp('2.1%'),
        backgroundColor: colors.secondColor,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: hp('1.8%'),
        textAlign: 'center',
        color: colors.white,
        textTransform: 'uppercase',
        letterSpacing:hp('.15%')
    },
    imageStyle: {
        width: '100%',
        height: '100%',
        resizeMode: 'stretch'
    },
    inputStyle:{
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth:hp('.2%'),
        borderBottomColor:colors.inputCon,
        paddingVertical:0
    },
    inputIcon:{
        width:hp('2.8%'),
        height:hp('3%')
    },
    input:{
        flex:1,
        paddingLeft:hp('1.3%'),
        fontSize:hp('1.9%'),
        color:colors.inputCon,
        paddingBottom:hp('.5%')
    },
});