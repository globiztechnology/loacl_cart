import React, { useEffect } from 'react';
import {
  View,
  StatusBar,
  BackHandler
} from 'react-native';
import { Provider } from 'react-redux';
import store from './src/redux/store/index';
import AppContainer from './src/navigation/index';

export default class App extends React.Component {
  
  render() {
    return (
      <Provider style={{ flex: 1 }} store={store}>
        <StatusBar hidden={true} />
        <AppContainer />
      </Provider>
    );
  }
};